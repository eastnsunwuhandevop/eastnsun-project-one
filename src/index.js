import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route } from "react-router-dom";
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import 'antd/dist/antd.css';
// import RegIndex from './container/register/index';
import IndexPage from './container/index/index'
import AboutIndex from './container/about/index'
import CareersIndex from './container/Careers/index'
import Login from './container/login/Login';
import Identify from './container/identify/Identify';
import Profession from './container/profession/Profession';
import Detail from './container/detail/Detail';

import GOlang_developer from './container/Careers/Content/GOlang_developer'
import Python_developer from './container/Careers/Content/python_developer'
import GOlang_developer_cn from './container/Careers/Content/GOlang_developer_cn'
import Python_developer_cn from './container/Careers/Content/python_developer_cn'
import DemoIndex from './container/demo/index'
import Add from './container/demo/soi/Add';
import Item from './container/demo/soi/Item';
import Searchs from './container/demo/soi/Searchs';
import Invite from './container/demo/soi/Invite';
import Code001One from './container/demo/code001/Code001One';
import Code001Two from './container/demo/code001/Code001Two';
import Code001Three from './container/demo/code001/Code001Three';
import Code001Four from './container/demo/code001/Code001Four';
import Code001Five from './container/demo/code001/Code001Five';
import Warning from './container/demo/code001/Warning';
import Sign from './container/demo/code001/Sign';
import InformationIndex from './container/demo/soi/InformationIndex';


ReactDOM.render(
  <HashRouter>
    <div>
      <Route exact path="/" component={IndexPage} />
      {/* <Route exact path="/register" component={RegIndex} /> */}
      <Route exact path="/ourstory" component={AboutIndex} />
      <Route exact path='/profession' component={Profession} />
      <Route exact path='/detail' component={Detail}/>
      <Route exact path='/login' component={Login}/>
      <Route exact path='/identify' component={Identify}/>
      <Route exact path='/career' component={CareersIndex}/>
      <Route exact path='/demo' component={DemoIndex}/>

      <Route exact path="/zh/ourstory" component={AboutIndex}/>
      <Route exact path="/zh/" component={IndexPage}/>
      <Route exact path='/zh/career' component={CareersIndex}/>
      <Route exact path='/career/go' component={GOlang_developer}/>
      <Route exact path='/career/python' component={Python_developer}/>
      <Route exact path='/zh/career/go' component={GOlang_developer_cn}/>
      <Route exact path='/zh/career/python' component={Python_developer_cn}/>


      <Route exact path="/add" component={Add}/>
      <Route exact path="/item" component={Item}/>
      <Route exact path="/search" component={Searchs}/>
      <Route exact path="/code001one" component={Code001One}/>
      <Route exact path="/code001Two" component={Code001Two}/>
      <Route exact path="/code001Three" component={Code001Three}/>
      <Route exact path="/code001Four" component={Code001Four}/>
      <Route exact path="/code001Five" component={Code001Five}/>
      <Route exact path="/warning" component={Warning}/>
      <Route exact path="/sign" component={Sign}/>
      <Route exact path="/invite" component={Invite}/>
      <Route exact path="/InformationIndex" component={InformationIndex}/>
      

    </div>
  </HashRouter>,
  document.getElementById("root")
);
registerServiceWorker();