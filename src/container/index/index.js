import React, {Component} from 'react'

import Navbar from '../../component/index/navbar/Navbar'
import Banner from '../../component/index/banner/Banner'
import Bannersec from '../../component/index/bannersec/Bannersec'
import Bannerthird from '../../component/index/bannerthird/Bannerthird'
import Footer from '../../component/index/footer/Footer'
import {INDEX_PAGE} from '../../sources/source'

class IndexPage extends Component{
    switchLang(){
        const lan = this.props.location.pathname.substr(0, 3)
        if(lan === '/zh')
        {  
            const newAddress = this.props.location.pathname.slice(3, this.props.location.pathname.length);
            this.props.history.push(newAddress)
        } else {
            this.props.history.push('/zh' + this.props.location.pathname)
        }
    }

    renderIndex(e){
        return <div>
            <Navbar source={e.header} switchLang={this.switchLang.bind(this)}/>
            <Banner  source={e.banner1}/>
            <Bannersec  source={e.banner2}/>
            <Bannerthird  source={e.banner3}/>
            <Footer  source={e.footer} switchLang={this.switchLang.bind(this)}/>
            </div>
    }
    
    render(){
        const lan = this.props.location.pathname.substr(0, 3)
        const language = lan === '/zh' ?  INDEX_PAGE.cn : INDEX_PAGE.en
        return <div>
            {this.renderIndex(language)}
        </div>
    }
}

export default IndexPage