import React from 'react';
import './Identify.css';
import {Row, Col} from 'antd';
class Identify extends React.Component {
	click(){
		this.props.history.push('Profession')
	}
	render(){
		return(
             <div className="Identify-container">
                <Row type="flex" justify="center" align="middle">
                  <Col className="Identify-title">Choose your identify</Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col>
                     <button className="Identify-left" >Buyer</button>
                  </Col>
                  <Col>
                     <button className="Identify-right">Seller</button>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col>
                     <button className="Identify-left-bottom" onClick={()=>this.click()}>Company</button>
                  </Col>
                  <Col>
                     <button className="Identify-right-bottom">Government Agencies and Industry Bodies</button>
                  </Col>
                </Row>
             </div>
			);
	}
}
export default Identify;