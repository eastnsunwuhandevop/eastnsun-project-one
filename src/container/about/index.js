import React, { Component } from "react";
import Banner from "../../component/about/banner/Banner";
import Navbar from "../../component/index/navbar/Navbar";
import Footer from "../../component/index/footer/Footer";
import { INDEX_PAGE } from "../../sources/source";
class AboutIndex extends Component {
  switchLang() {
    const lan = this.props.location.pathname.substr(0, 3);
    if (lan === "/zh") {
      const newAddress = this.props.location.pathname.slice(
        3,
        this.props.location.pathname.length
      );
      this.props.history.push(newAddress);
    } else {
      this.props.history.push("/zh" + this.props.location.pathname);
    }
  }
  renderIndex(e, l) {
    return (
      <div>
        <Navbar source={e.header} switchLang={this.switchLang.bind(this)} />
        <Banner lang={l} />
        <Footer source={e.footer} switchLang={this.switchLang.bind(this)} />
      </div>
    );
  }
  render() {
    const lan = this.props.location.pathname.substr(0, 3);
    const language = lan === "/zh" ? INDEX_PAGE.cn : INDEX_PAGE.en;
    const switchLan = lan === "/zh" ? false : true;
    return <div>{this.renderIndex(language, switchLan)}</div>;
  }
}

export default AboutIndex;
