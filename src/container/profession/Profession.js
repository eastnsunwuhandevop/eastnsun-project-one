import React from 'react';
import {Row, Col} from 'antd';
import './Profession.css';

class Profession extends React.Component {
	click(){
		this.props.history.push('Detail')
	}
   render(){
   	return(
       <div className="Profession-container">
            <Row type="flex" justify="center" align="middle">
              <Col className="Profession-title">Choose your profession</Col>
            </Row>
            <Row type="flex" justify="center" align="middle">
                  <Col>
                     <button className="Profession-left" onClick={()=>this.click()}>Agency</button>
                  </Col>
                  <Col>
                     <button className="Profession-right">Lawyer</button>
                  </Col>
                </Row>
                <Row type="flex" justify="center" align="middle">
                  <Col>
                     <button className="Profession-left-bottom">Broker</button>
                  </Col>
                  <Col>
                     <button className="Profession-right-bottom">Barrister</button>
                  </Col>
                </Row>
       </div>         
   		);
   }
} 
export default Profession;