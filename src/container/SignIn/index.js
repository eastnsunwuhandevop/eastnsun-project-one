import React from "react";
import logo from "../../assets/img/logo.png";
import "./index.css";
import axios from "axios";
import { Form, Input, Button, Alert } from "antd";
const FormItem = Form.Item;
const reg = /[a-zA-Z0-9]{1,10}@[a-zA-Z0-9]{1,5}\.[a-zA-Z0-9]{1,5}/;
const loginUrl = "http://192.168.0.107:3000/login";

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      visible: false,
      description: ""
    };
  }
  handleClose = () => {
    this.setState({ visible: false });
  };
  //提交
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.closeProgress(true);
        const loginEmail = values.email;
        const loginPassword = values.password;
        this.login(loginEmail, loginEmail).then(res => {
          console.log(res);
          if (res.status === 200) {
            if (res.data.code === 1) {
              this.setState({
                visible: true,
                description: res.data.msg,
                loading: false
              });
            }
          }
        });
      }
    });
  };
  //登录
  login(email, password) {
    return axios
      .post(
        loginUrl,
        {
          openid: email,
          login_token: password
        },
        { "Content-Type": "application/json" }
      )
      .then(function(response) {
        console.log(response);
        return response;
      })
      .catch(function(error) {
        return error;
      });
  }

  closeProgress = e => {
    this.setState({
      loading: e
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="Login-body">
          <div className="Login-container">
            <div className="Login-logo">
              <img src={logo} />
            </div>
            <hr className="Login-divider" />
            <span className="Login">Login</span>
            <div className="Login-email-container">
              <div className="Login-email">Email</div>
              <FormItem>
                {getFieldDecorator("email", {
                  rules: [
                    {
                      type: "email",
                      message: "The input is not valid E-mail!"
                    },
                    {
                      required: true,
                      message: "Please input your E-mail!"
                    }
                  ]
                })(
                  <div className="Login-email-input">
                    <Input size="large" />
                  </div>
                )}
              </FormItem>
            </div>
            <div className="Login-password-container">
              <div className="Login-pwd">
                <div className="Login-password">Password</div>
                <a className="Login-forget-password" href="">
                  Forgot password
                </a>
              </div>

              <FormItem>
                {getFieldDecorator("password", {
                  rules: [
                    { required: true, message: "Please input your Password!" }
                  ]
                })(
                  <div className="Login-password-input">
                    <Input size="large" />
                  </div>
                )}
              </FormItem>
            </div>

            <div>
              {this.state.visible ? (
                <Alert
                  description={this.state.description}
                  type="error"
                  closable
                  afterClose={this.handleClose}
                />
              ) : null}
            </div>
            <div className="Login-submit-container">
              <Button
                type="primary"
                size="large"
                loading={this.state.loading}
                htmlType="submit"
                className="Login-submit"
              >
                Submit
              </Button>
            </div>
          </div>
        </div>
      </Form>
    );
  }
}
const LoginForm = Form.create()(SignIn);
export default LoginForm;
