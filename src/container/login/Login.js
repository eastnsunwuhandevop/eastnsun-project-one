import React from 'react';
import { Link } from 'react-router-dom'

import logo from '../../assets/img/logo.png';
import './Login.css'
import {Form, Input,Button} from 'antd';
const FormItem = Form.Item; 
const reg=/[a-zA-Z0-9]{1,10}@[a-zA-Z0-9]{1,5}\.[a-zA-Z0-9]{1,5}/;;
class Login extends React.Component {
  constructor(props) {
	  super(props);
	  this.state = {
      flag:false,
      email:"",
      password:"",
	  };
  }
  onChangeEmail=(e)=>{
    this.setState({
      email:e.target.value,
    });
  }
  onChangePassword=(e)=>{
    this.setState({
      password:e.target.value,
    });
  }
  //提交
  submit=()=>{
    //  this.setState({flag:true});
    this.props.history.push('./search')
  }
  //email空判断
  setEmailEmpty(){
     if(this.state.flag&&this.state.email===""){
      return  <FormItem
                validateStatus="error"
                help="Please enter your E-mail"
                >    
                <div className="Login-email-input">
                    <Input size="large" onChange={this.onChangeEmail}/>
                </div>
              </FormItem>  
       }else{
         if(this.state.flag&&!reg.test(this.state.email)){
          return <FormItem
                  validateStatus="error"
                  help="Please enter your correct E-mail"
                  >    
                  <div className="Login-email-input">
                      <Input size="large" onChange={this.onChangeEmail}/>
                  </div>
                </FormItem>  
         }else{
          return <FormItem
                  >    
                  <div className="Login-email-input">
                      <Input size="large" onChange={this.onChangeEmail}/>
                  </div>
                </FormItem>  
         }
       }
      }
      //password空判断
      setPasswordEmpty(){
        if(this.state.flag&&this.state.password===""){
         return  <FormItem
                   validateStatus="error"
                   help="Please enter your password"
                   >    
                   <div className="Login-password-input">
                       <Input size="large" onChange={this.onChangePassword}/>
                   </div>
                 </FormItem>  
          }else{
             return <FormItem
                       >    
                       <div className="Login-password-input">
                           <Input size="large" onChange={this.onChangePassword}/>
                       </div>
                     </FormItem>  
          }
         }
  
  render(){
    return(
   <Form>   
      <div className="Login-body">   
        <div className="Login-container">
            <Link to={{pathname:'/'}}><div className="Login-logo"><img src={logo}  alt=""/></div></Link>
            <hr className="Login-divider"/>
            <span className="Login">Login</span>
            <div className="Login-email-container">
              <div className="Login-email">Email</div>
                {this.setEmailEmpty()}
            </div>
              <div className="Login-password-container">
              <div className="Login-pwd">
                  <div className="Login-password">Password</div>
                    <a className="Login-forget-password" href="">Forgot password</a>
              </div>
              {this.setPasswordEmpty()}
            </div>
            <div className="Login-submit-container">
                <Button type="primary" size="large" onClick={this.submit} className="Login-submit">Submit</Button>
            </div>
        </div>
      </div> 
    </Form>
      );
  }
}
export default Login;
 