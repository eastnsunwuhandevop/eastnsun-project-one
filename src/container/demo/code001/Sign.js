import React from 'react';
import './Code001.css';
import { Icon} from 'antd';
class Sign extends React.Component{
    render(){
        return(
            <div className="Code-container">
               <div className="Code-sign-container">
                    JOE
               </div>
               <div className="Code-modfiy-container">
                    <div className="Code-bottom" onClick={this.return}>
                        <Icon type="left" />return
                    </div>
                    <div className="Code-bottom" onClick={this.continue}>
                        Confirm and Continue<Icon type="right" />
                    </div>
                </div>
            </div>
        );
    }
}
export default Sign;