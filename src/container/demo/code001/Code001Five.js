import React from 'react';
import './Code001.css';
import { Icon,Input, Checkbox,DatePicker, Button} from 'antd';
const { TextArea } = Input;
class Code001Five extends React.Component{
    generate=()=>{
        this.props.history.push('warning');
    }
    render(){
        return(
            <div className="Code-container">
              <div className="Code-two-container">
                 <div className="Code-two-title">Marketing information to be confirmed</div>
                 <div className="Code-input-container">
                     <div className="Code-modfiy">Marketing Expenses</div>
                     <div className="Code-hint">(including GST)</div>
                 </div>
                 <div className="Code-hint-container">
                     <div className="Code-hint">Advertsing</div>
                        <div className="Code-four-input">
                            <Input prefix={<i class="iconfont">&#xe66d;</i>}/>
                        </div>
                  </div>
                  <div className="Code-hint-container">
                     <div className="Code-hint">Other Expenses</div>
                        <div className="Code-four-input">
                            <Input prefix={<i class="iconfont">&#xe66d;</i>}/>
                        </div>
                  </div>
                  <div className="Code-hint-container">
                     <div className="Code-hint">TOTAL</div>
                        <div className="Code-four-input">
                            <Input prefix={<i class="iconfont">&#xe66d;</i>}/>
                        </div>
                  </div>
                  <div className="Code-input-container">
                     <div className="Code-modfiy">Marketing Expenses are payable on</div>
                 </div>
                 <div className="Code-price-container">
                    <div className="Code-checkbox-container">
                        <Checkbox/>
                        <div className="Code-table-bottom-title Code-hint-left">*the singing of this Authority</div>
                    </div>
                    <div className="Code-checkbox-container">
                        <Checkbox/>
                        <div className="Code-table-bottom-title Code-hint-left">*written request</div>
                        <div className="Code-right-hint">(*choose the one that apply)</div>
                    </div>
                 </div>
                 <div className="Code-hint-container">
                     <div className="Code-hint">DATE</div>
                        <div className="Code-four-input">
                            <DatePicker />
                        </div>
                  </div>
                  <div className="Code-four-content-container Code-note-hight">
                      <div className="Code-sign">Agent Digital Signature</div>
                      <div><TextArea rows={4} placeholder="JOE Z"/></div>
                  </div>
              </div>
                 <div className="Code-modfiy-container">
                      <div><button className="Code-button" onClick={this.generate}>Generate PDF</button></div>
                      <div><button className="Code-button">Generate QR Code</button></div>
                  </div>
            </div>
        );
    }
}
export default Code001Five;