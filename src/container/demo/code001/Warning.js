import React from 'react';
import './Code001.css';
class Warning extends React.Component{
    cancel=()=>{
        this.props.history.goBack();
    }
      render(){
          return(
            <div className="Code-container">
                <div className="Code-two-container">
                   <div className="Code-two-title">WARNING</div>
                </div>
                <div className="Code-warning-container Code-warning-top">The statemet of information will be used as an unchangeable</div>
                <div className="Code-warning-container Code-note-hight">information.Please double check the information is correct or not</div>
                 <div className="Code-modfiy-container">
                      <div><button className="Code-warning-button" onClick={this.cancel}>CANCEL</button></div>
                      <div><button className="Code-warning-button">CONFIRM</button></div>
                  </div>
            </div>
          );
      }
}
export default Warning;