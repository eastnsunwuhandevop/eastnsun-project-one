import React from 'react';
import './Code001.css';
import { Icon,Input} from 'antd';
class Code001Three extends React.Component{
    constructor(){
        super();
        this.state={
            index:-1,
        }
    }
    return=()=>{
        // this.props.history.goBack();
        this.props.history.push({
            pathname:"./Code001Two",
            index:this.state.index,
        });
    }
    continue=()=>{
        this.props.history.push({
            pathname:"./Code001Four",
            index:this.state.index,
        });
    }
    renderACN(e){
        switch(e){
            case 2:
            case 3:
               return <div className="Code-modfiy-container">
                        <div className="Code-input-name">ACN</div>
                        <div className="Code-input"><Input /></div>
                    </div>
            break;
        }
    }
    renderABN(e){
        switch(e){
            case 2:
            case 3:
               return <div className="Code-modfiy-container">
                        <div className="Code-input-name">ABN</div>
                        <div className="Code-input"><Input /></div>
                    </div>
            break;
        }
    }
    render(){
        const self=this.props.location.index;
        this.state.index=self;
        return(
            <div className="Code-container">
                <div className="Code-two-container">
                    <div className="Code-two-title">Vendor's information to be confirmed</div>
                    <div className="Code-two-content-container">  
                        <div className="Code-modfiy-container">
                            <div className="Code-modfiy">Agent</div>
                            <div className="Code-modify-icon"><Icon type="form" /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Name</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Address</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        {this.renderACN(self)}
                        {this.renderABN(self)}
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Phone</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Mobile</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Email</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-bottom" onClick={this.return}>
                                <Icon type="left" />return
                            </div>
                            <div className="Code-bottom" onClick={this.continue}>
                                Confirm and Continue<Icon type="right" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Code001Three;
