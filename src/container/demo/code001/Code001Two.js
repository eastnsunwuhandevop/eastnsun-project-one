import React from 'react';
import './Code001.css';
import { Icon,Input} from 'antd';
class Code001Two extends React.Component{
    constructor(){
        super();
        this.state={
            index:-1,
        }
    }
    return=()=>{
        // this.props.history.goBack();
        this.props.history.push({
            pathname:'./Code001One',
            index:this.state.index,
        });
    }
    continue=()=>{
        this.props.history.push({
            pathname:'./Code001Three',
            index:this.state.index,
        });
    }

    renderAgent(e){
          switch(e){
              case 1:
              case 2:
              return <div className="Code-modfiy">Agent</div>
              break;
              case 3:
                  return <div className="Code-modfiy">Agent(Administrator)</div>
              break;
          }
    }
    renderAttention(e){
        switch(e){
            case 1:
            case 2:
               return <div className="Code-input"><Input /></div> 
            break;
            case 3:
                return   <div className="Code-attention-container">
                            <div className="Code-four-input"><Input /></div>
                            <div className="Code-input-name">Administrator</div>
                        </div>   
            break;
        }
    }

    renderACN(e){
        switch(e){
            case 2:
            case 3:
               return <div className="Code-modfiy-container">
                        <div className="Code-input-name">ACN</div>
                        <div className="Code-input"><Input /></div>
                    </div>
            break;
        }
    }
    renderABN(e){
        switch(e){
            case 2:
            case 3:
               return <div className="Code-modfiy-container">
                        <div className="Code-input-name">ABN</div>
                        <div className="Code-input"><Input /></div>
                    </div>
            break;
        }
    }
    renderFAX(e){
        switch(e){
            case 2:
            case 3:
               return <div className="Code-modfiy-container">
                        <div className="Code-input-name">FAX</div>
                        <div className="Code-input"><Input /></div>
                    </div>
            break;
        }
    }
    render(){
        const self=this.props.location.index;
        this.state.index=self;
        return(
            <div className="Code-container">
                <div className="Code-two-container">
                    <div className="Code-two-title">Agnet's information to be confirmed</div>
                    <div className="Code-two-content-container">  
                        <div className="Code-modfiy-container">
                            {this.renderAgent(self)}
                            <div className="Code-modify-icon"><Icon type="form" /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Name</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Address</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        {this.renderACN(self)}
                        {this.renderABN(self)}
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Attention</div>
                           {this.renderAttention(self)}
                        </div>
                        {this.renderFAX(self)}
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Phone</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Mobile</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-input-name">Email</div>
                            <div className="Code-input"><Input /></div>
                        </div>
                        <div className="Code-modfiy-container">
                            <div className="Code-bottom" onClick={this.return}>
                                <Icon type="left" />return
                            </div>
                            <div className="Code-bottom" onClick={this.continue}>
                                Confirm and Continue<Icon type="right" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Code001Two;
