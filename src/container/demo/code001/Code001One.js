import React from 'react';
import './Code001.css';
import { Icon} from 'antd';
class Code001One extends React.Component {
    constructor(){
        super();
        this.state={
            index:-1,
        };
    }
    return=()=>{
        // this.props.history.goBack();
        this.props.history.push('InformationIndex');
    }
    continue=()=>{
        this.props.history.push({
            pathname:'./Code001Two',
            index:this.state.index,
        })
    }
    //title
    renderTitle(e){
        switch(e){
            case 1:
              return <span className="Code-top-title">Code001</span>
            break;
            case 2:
               return <span className="Code-top-title">Code002</span>
            break;
            case 3:
               return <span className="Code-top-title">Code003</span>
            break;
            case 5:
            return <span className="Code-top-title">Code118</span>
         break;
        }
    }

    renderHint(e){
        switch(e){
            case 2:
            case 3:
            case 5:
               return <div className="Code-one-hint-container">
                           The Real Estate Institute of Victoria Ltd | www.reiv.com.au | ABN 81 004 210 897
                     </div>;
            break;
        }
    }

    renderWarning(e){
        switch(e){
            case 5:
               return <div className="Code-one-hint-container">
                        <div className="Code-warning-container">
                            WARNING TO ESTATE AGENTS DO NOT USE THIS CONTRACT FOR SALES OF OFF THE PLAN PROPERTIES UNLESS IT HAS BEEN PREPARES BY A LEGAL PRACTITIONER
                        </div>
                      </div>;
            break;
        }
    }
    renderLable(e){
        switch(e){
            case 1:
            case 2:
            case 3:
              return  <div className="Code-one-content-container">Particulars of Appointment</div>
            break;
            case 5:
               return  <div className="Code-one-content-container">Part 1 of the form of contract published by the Law Institude of Victoria Limited and the Real Estate Institute of Victoria Ltd</div>
            break;
        }
    }

    render(){
        const self=this.props.location.index;
        this.state.index=self;
        return(
            <div className="Code-container">
               <div className="Code-one-container">
                <div className="Code-one-title-container">
                    <div className="Code-one-left-container">
                        {this.renderTitle(self)}
                        <span className="Code-bottom-title">General Sale Authority</span>
                    </div>
                    <div className="Code-left-title">REIV</div>
                </div>
                  {this.renderHint(self)}
                  {this.renderWarning(self)}
                  {this.renderLable(self)}
               </div> 
               <div className="Code-modfiy-container">
                    <div className="Code-bottom" onClick={this.return}>
                        <Icon type="left" />return
                    </div>
                    <div className="Code-bottom" onClick={this.continue}>
                        Confirm and Continue<Icon type="right" />
                    </div>
                </div>
            </div>
        );
    }
}
export default Code001One;