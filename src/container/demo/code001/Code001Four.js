import React from 'react';
import './Code001.css';
import { Icon,Input, Checkbox,DatePicker} from 'antd';
const InputGroup = Input.Group;
class Code001Four extends React.Component{
    constructor(){
        super();
        this.state={
            index:-1,
        }
    }
    return=()=>{
        this.props.history.goBack();
        this.props.history.push({
            pathname:"./Code001Three",
            index:this.state.index,
        });
    }
    continue=()=>{
        this.props.history.push({
            pathname:"./Code001Five",
            index:this.state.index,
        });
    }

    renderExclusive(e){
        switch(e){
            case 2:
            return <div className="Code-input-container">
                        <div className="Code-day-container">
                            <div className="Code-modfiy">Exclusive authority period</div>
                            <div className="Code-day-input"><Input/></div>
                            <div className="Code-modfiy">days</div>
                        </div>
                    </div>
            break;
            case 3:
              return <div className="Code-input-container">
                         <div className="Code-input-name">Exclusive authority period until</div>
                            <div className="Code-day-input"><Input/></div>
                            <div className="Code-input-name">days after the action date.</div>
                     </div>
            break;
        }
    }
    renderContinuing(e){
        switch(e){
            case 2:
            return  <div className="Code-input-container">
                        <div className="Code-day-container">
                            <div className="Code-modfiy">Continuing authority period</div>
                            <div className="Code-day-input"><Input/></div>
                            <div className="Code-modfiy">days</div>
                        </div>
                    </div>
            break;
            case 3:
              return <div className="Code-input-container">
                         <div className="Code-input-name">Continuing authority period</div>
                            <div className="Code-day-input"><Input/></div>
                            <div className="Code-input-name">days from the end of exclusive.</div>
                     </div>
            break;
        }
    }

    renderDate(e){
        switch(e){
            case 3:
               return <div className="Code-input-container">
                         <div className="Code-input-name">Aution date</div>
                         <div><DatePicker /></div>
                      </div>
            break;
        }
    }
    renderTableTitle(e){
        switch(e){
            case 1:
              return <div className="Code-table-title">A *commission(including GST) being the following % of the sale price</div>
            break;
            case 2:
            case 3:
              return <div className="Code-table-title">A *commission(including GST) being the following % of the sale price or caculated as follows</div>
            break;
        }
    }
    renderCommission(e){
        switch(e){
           case 2:
           case 3:
             return <div className="Code-commission-title">
                      (*Commission:Vendor please read the payment of commission terms at page2,Item 1,before you sign this Authority)
                    </div>
           break;
        }
    }
    renderGST(e){
         switch(e){
             case 2:
             case 3:
                return  <div className="Code-left-bottom-row-container">
                            <div className="Code-gst-container">
                                <Checkbox/>
                                <div className="Code-table-bottom-title">*Not subject to GST</div>
                            </div>
                        </div>
             break;
         }
    }
    renderCommissionTitle(e){
        switch(e){
            case 2:
            case 3:
               return  <div className="Code-table-top-container">
                            <div className="Code-title">Commission</div>
                        </div>
            break;
        }
    }
    render(){
        const self=this.props.location.index;
        this.state.index=self;
        return(
            <div className="Code-container">
              <div className="Code-two-container">
                <div className="Code-two-title">Property information to be confirmed</div>
                    <div className="Code-input-container">
                        <div className="Code-modfiy">Property</div>
                    </div>
                    <div className="Code-input-container">
                        <div className="Code-four-content-container">
                            <div className="Code-modfiy">Property Address</div>
                            <div className="Code-four-input"><Input/></div>
                        </div>
                    </div>
                    <div className="Code-input-container">
                        <div className="Code-four-content-container">
                            <div className="Code-modfiy">with chattelts being</div>
                            <div className="Code-four-input"><Input/></div>
                        </div>
                    </div>

                    {this.renderExclusive(self)}
                    {this.renderContinuing(self)}
                    {this.renderDate(self)}
                    <div className="Code-input-container">
                        <div className="Code-hint">The Property is being sold</div>
                    </div>
                    <div className="Code-input-container">
                        <Checkbox/>
                        <div className="Code-hint Code-hint-left">with vacnt possession or</div>
                    </div>
                    <div className="Code-input-container">
                        <Checkbox/>
                        <div className="Code-hint Code-hint-left">subject to a tenancy on payment of</div>
                    </div>
                    <div className="Code-input-container">
                        <Checkbox/>
                        <div className="Code-hint Code-hint-left">full purchase price</div>
                    </div>
                    <div className="Code-hint-container">
                        <div className="Code-title">OR </div>
                        <div className="Code-hint">upon terms on payment of</div>
                        <div className="Code-price-input"><Input prefix={<i class="iconfont">&#xe67b;</i>}/></div>
                    </div>
                    <div className="Code-hint-container">
                        <div></div>
                        <div className="Code-hint">full deposit and the sum of</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b;</i>}/>
                        </div>
                    </div>
                    <div className="Code-price-container Code-price-width">
                        <div className="Code-title">Vendor's price</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b;</i>}/>
                        </div>
                        <div className="Code-title">Payable in</div>
                        <div className="Code-price-input">
                            <Input/>
                        </div>
                        <div className="Code-title">days</div>
                    </div>
                    <div className="Code-note-container Code-note-hight">
                        <div className="Code-title">Agent's estimate of selling price</div>
                        <div className="Code-hint">(Section 47A of the Estate Agents Act 1980).</div>
                    </div>
                    <div className="Code-note-container">
                        <div className="Code-hint">Note: If a price range is specified, the differnce between the upper and lower amounts cannot </div>
                    </div>
                    <div className="Code-note-container">
                        <div className="Code-hint">Dollar amount of estimated commission</div>
                    </div>
                    <div className="Code-price-container Code-price2-width">
                        <div className="Code-hint">Single amount</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                        </div>
                        <div className="Code-hint">or between</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                        </div>
                        <div className="Code-hint">and</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                        </div>
                    </div>
                    {this.renderCommissionTitle(self)}
                    <div className="Code-table-top-container">
                        <div className="Code-title">A fixed commission</div>
                        <div className="Code-hint">(including GET)</div>
                        <div className="Code-title">of</div>
                        <div className="Code-price-input">
                            <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                        </div>
                        <div className="Code-title">OR </div>
                    </div>
     
                    <div className="Code-table-container">
                        <div className="Code-table">
                           <div className="Code-table-content-container">
                               {this.renderTableTitle(self)}
                                <div className="Code-table-input"><Input/></div>
                                <div className="Code-note-container">
                                    <div className="Code-table-lable Code-top">Dollar amount of estimated commission</div>
                                </div>
                                <div className="Code-price-container">
                                    <div className="Code-left-input">
                                        <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                                    </div>
                                    <div className="Code-table-lable">including GST of</div>
                                    <div className="Code-right-input">
                                        <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                                    </div>
                                </div>
                                <div className="Code-price-container">
                                   <div className="Code-left-container">
                                      <div className="Code-left-row-container">
                                        <div className="Code-table-bottom-title">if sold at a</div>
                                        <div className="Code-gst-container">
                                            <Checkbox/>
                                            <div className="Code-table-bottom-title">*GST inclusive</div>
                                        </div>
                                      </div>
                                      <div className="Code-left-bottom-row-container">
                                       <div className="Code-gst-container">
                                            <Checkbox/>
                                            <div className="Code-table-bottom-title">*GST exclusive price/</div>
                                        </div>
                                      </div>
                                     {this.renderGST(self)}
                                   </div>
                                   <div className="Code-right-container">
                                       <div className="Code-table-lable Code-right">of</div> 
                                    <div className="Code-right-input">
                                        <Input prefix={<i class="iconfont">&#xe67b; </i>}/>
                                    </div>
                                   </div>
                                </div>
                                <div className="Code-note-container Code-top">
                                    <div className="Code-table-lable">(*choose the one that apply)</div>
                                </div>
                                {this.renderCommission(self)}
                            </div>
                        </div>
                    </div>
                   </div> 
                    <div className="Code-modfiy-container">
                        <div className="Code-bottom" onClick={this.return}>
                            <Icon type="left" />return
                        </div>
                        <div className="Code-bottom" onClick={this.continue}>
                            Confirm and Continue<Icon type="right" />
                        </div>
                    </div>
            </div>
        );
    }
}
export default Code001Four;