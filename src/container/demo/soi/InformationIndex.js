import React from 'react';
import './Soi.css';
import Header from './Header';
import Information from './Information';
import InformationHeader from './InformationHeader';
import { Icon,Button} from 'antd';
import {HEADER,FORM_LIST} from './data';

class InformationIndex extends React.Component {
    constructor() {
        super();
        this.state = {
          step: 0,
          payload: []
        };
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
      }

  handleNext() {
    this.setState({
      step: this.state.step + 1
    });
  }

  handlePrev() {
      if(this.state.step==0){
        // this.props.history.goBack();
        this.props.history.push("./Item");
      }else{
            this.setState({
            step: this.state.step - 1
            });
        }
  }
  handleSetPayload(e) {
    if(e.index!=0){
       this.props.history.push({
         pathname:'./Code001One',
         index:e.index
       });
    }else{
    this.setState({
      payload: [...this.state.payload, e],
      step: this.state.step + 1
        });
    }
  }

  renderTitle(self){
    return HEADER.map((v,index) => {
        if(self.step==index){
            return ( 
                <Header
                key={index}
                header={v.header}
                class={v.class}
                />
            );
        }
    });
  }

    renderHeader(self){
        return HEADER.map((v,index) => {
            if(self.step==index){
                return ( 
                    <InformationHeader
                    key={index}
                    title={v.title}
                    subTitle={v.subTitle}
                    progress={v.progress}
                    total={v.total}
                    />
                );
            }
        });
    }

    renderContent(self){
        const listLength = FORM_LIST(this).length
        return <Information key={self.step} data={FORM_LIST(this)[self.step]}/>;
    }

    renderButton(){
       if(this.state.step<2||this.state.step===4){
           return <div className="information-return-container">
                        <Button type="primary" className="information-button" onClick={this.handlePrev}>
                            Return
                        </Button>
                  </div>
       }else if(this.state.step<4){
        return <div className="information-button-container">
                    <Button type="primary" className="information-button" onClick={this.handlePrev}>
                        <Icon type="left" />Previous
                    </Button>
                    <Button type="primary" className="information-button" onClick={this.handleNext}>
                        Next<Icon type="right" />
                    </Button>
                </div>
       }
    }
    render(){
        const self=this.state;
        return(
            <div className="Sio-container">
               <div className="Sio-information-container">
                    {this.renderHeader(self)}
                    {this.renderContent(self)}
                    {this.renderButton()}
               </div>
            </div>
        );
    }
}
export default InformationIndex;
