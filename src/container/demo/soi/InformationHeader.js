import React from 'react';
import { Steps } from "antd";
const Step = Steps.Step;
class InformationHeader extends React.Component {
    renderProgress(e) {
        var result = [];
        for (var i = 0; i < e; i++) {
          result.push(<Step key={i}/>);
        }
        return result;
      }
    
      render() {
        const headerTitle = e => (
          <div className="information-title">
            <p>{e}</p>
          </div>
        );

        const headerSubTitle = (e) => (
            <div className="information-sub-title">
              {e ? (
                <p>
                   {e}
                </p>
              ) : null}
            </div>
          );
    
        const headerProgress = (e, t) => (
          <div className="reg-progress">
            <div>
              <Steps size="small" className="reg-progress-content" current={e - 1}>
                {this.renderProgress(t)}
              </Steps>
            </div>
          </div>
        );
    
        const _self = this.props;
        console.log("------"+_self.subTitle);
        return (
          <div>
            {headerTitle(_self.title, _self.total)}
            {headerProgress(_self.progress, _self.total)}
            {headerSubTitle(_self.subTitle)}
          </div>
        );
      }
}
export default InformationHeader;