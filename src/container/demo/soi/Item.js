import React from 'react';
import {Icon,Button,Tabs} from 'antd';
const TabPane = Tabs.TabPane;

const LIST=[
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        buttonName:"Action",
        progress:"pending",
    },
];
const LIST2=[
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
    {
        title:"U 2 2 Catalina Ave Ashburon",
        progress:"processed",
    },
];
class Item extends React.Component {

    action=()=>{
        this.props.history.push('InformationIndex')
    }

    renderPending(){
       return LIST.map(v=>{
             return(
                   <div className="Sio-info">  
                        <div className="Sio-name">{v.title}</div>
                            <div><Button className="Sio-button" onClick={this.action}>{v.buttonName}</Button></div>
                            <div className="Sio-progress">{v.progress}</div>
                    </div>
             );
        });
    }
    renderProcessed(){
        return LIST2.map(v=>{
              return(
                    <div className="Sio-info2">  
                         <div className="Sio-name">{v.title}</div>
                             <div className="Sio-progress">{v.progress}</div>
                     </div>
              );
         });
     }
    render(){
        return(
            <div className="Sio-container">  
                <div className="Sio-property-container">  
                    <div className="Sio-detail-title">Property</div>
                </div>  
                <div className="Sio-information-container">  
                    <Tabs defaultActiveKey="1" size="large" className="Sio-tab">
                        <TabPane tab="Pending" key="1">{this.renderPending()}</TabPane>
                        <TabPane tab="Processed" key="2">{this.renderProcessed()}</TabPane>
                    </Tabs>
                </div>
                
           </div>
        );
    }
}
export default Item;
            