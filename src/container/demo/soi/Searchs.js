import React from 'react';
import { Input ,Icon,Button} from 'antd';
import List from './List';
import './Soi.css';

const Search = Input.Search;
class Searchs extends React.Component {
  toAdd=()=>{
    this.props.history.push('Add')
  }
  confirm=()=>{
      this.props.history.push('Invite')
  }
  renderSearch(e){
   return <div>
              <Search className={e.data.classSearch} enterButton={e.data.enterButton} />
              <div className={e.data.classText} onClick={this.toAdd}>{e.data.title}</div>
           </div>
  }
      render(){
        const self=this.props;
          return(
            <div className="Sio-container">
             <div className="Sio-property-container">  
                <div className="Sio-detail-title">Property</div>
                 <div className="Sio-search-container">
                      <Search className="Sio-search"enterButton="Search" />
                      <div className="Sio-text" onClick={this.toAdd}>Can't find?</div>
                  </div>
             </div>
              <div className="Sio-information-container">
                  <div className="Sio-detail-container">
                       <div className="Sio-left-detail-container">
                          <div className="Sio-image"></div>
                          <div className="Sio-image-description">
                              <div className="Sio-image-title-top">172-176 Old Dandenong Road</div>
                              <div className="Sio-image-title-bottom">Heatherton Vic 3202</div>
                          </div>
                       </div>
                       <div className="Sio-right-detail-container">
                           <div className="Sio-house-detail-container">
                               <div className="Sio-detail-title">Genaral Features</div>
                               <div className="Sio-detail Sio-detail-top">Property Type:House</div>
                               <div className="Sio-detail Sio-detail-top">Bedrooms:7</div>
                               <div className="Sio-detail Sio-detail-top">Bedrooms:3</div>
                               <div className="Sio-detail Sio-detail-top">Land Size:0.82ha(2.01 acres)(approx)</div>
                               <div className="Sio-detail-title Sio-detail-title-top">Dutdoor Features</div>
                               <div className="Sio-detail Sio-detail-top">Garage Space:2</div>
                               <div className="Sio-detail-button" onClick={this.confirm}><Button type="primary">Confirm</Button></div>
                           </div>
                       </div>
                  </div>
              </div>
            </div>
          );
      }
}
export default Searchs;