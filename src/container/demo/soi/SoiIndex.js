import React from 'react';
import './Soi.css';
import { Input ,Icon,Button} from 'antd';
import Header from './Header';
import Searchs from './Searchs';
import List from './List';
import Add from './Add';
const Search = Input.Search;
const HEADER = [
    {
      header: "My Properties",
      class:"Sio-title",
    },
];
const SEARCH = [
        {
          title:"Can't find?",
          classText:"Sio-text",
          type: "search",
          enterButton: "Search",
          classSearch: "Sio-search",

        },
    ];
const SEARCH_LIST=[
    {
        title:"U 2 2 Catalina Ave Ashburon",
        editName:"Edit",
        editType:"form",
        removeName:"Remove",
        removeType:"close-circle",
        classTitle:"Sio-name",
        classIcon:"Sio-icon",
        btnName:"Action",
        classBtn:"Sio-button",
    },
];
const ADD_LIST=[
    {
        leftTitle:"Confirm Property Details below",
        rightTitle:"Update Address",
        classLeft:"Sio-confirm",
        classRight:"Sio-update",
    },
    {
        lable:"Unit",
        type:"input",
        classLable:"Sio-lable",
        classInput:"Sio-input",
    },
    {
        lable:"Street Name",
        type:"input",
        classLable:"Sio-lable",
        classInput:"Sio-input",
    },
    {
        lable:"Street Number",
        type:"input",
        classLable:"Sio-lable",
        classInput:"Sio-input",
    },
    {
        lable:"Postcode",
        type:"input",
        classLable:"Sio-lable",
        classInput:"Sio-input",
    },
    {
        lable:"State",
        type:"Select",
        classLable:"Sio-lable",
        classInput:"Sio-input",
        values:["VIC","NSW","TAS","NT","WA","QLD","SA"],
    },
    {
        btnName:"ADD",
        type:"primary",
        class:"Sio-add",
        size:"large",
    },
];
class SoiIndex extends React.Component {

    handleSetPayload(){
        // this.props.history.push('Add')
    }

      // render title
    renderSioTitle(_self) {
        return HEADER.map((v) => {
            return ( 
                <Header
                    data={v}
                />
            );
        });
    }
    renderSioSearch(_self){
        return SEARCH.map((v)=>{
            return(
                <Searchs data={v}/>
            );
        });
    }
    renderSioList(){
        return SEARCH_LIST.map((v)=>{
            return(
                <List key={v.lable} data={v}/>
            );
        });
    }
    renderSioAddList(){
        return ADD_LIST.map((v)=>{
            return(
                <Add key={v.lable} data={v}/>
            );
        });
    }

     render(){
        const _self = this.state;
        console.log(_self+"----------------------")
         return(
            <div className="Sio-container">
               {/* {this.renderSioTitle(_self)} */}
               {this.renderSioSearch(_self)}
               {/* {this.renderSioList(_self)} */}
               {/* {this.renderSioAddList(_self)} */}
            </div>
         );
     }
}
export default SoiIndex;