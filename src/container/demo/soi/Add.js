import React from 'react';
import {Input,Select,Button,Icon,Upload} from 'antd';
import Confirm from './Confirm';
const Option = Select.Option;
const Search = Input.Search;

class Add extends React.Component {
    cancel=()=>{
        this.props.history.goBack();
    }
    confirm=()=>{
        this.props.history.push('Item')
    }
     render(){
         return(
         <div className="Sio-container">    
            <div className="Sio-information-container">
            <div className="Sio-title-container">
                <div className="Sio-confirm">Confirm Property Details below</div>
            </div>    
            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Unit</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
                <div className="Sio-input-container">
                <div className="Sio-input-left-container">  
                   <div className="Sio-lable">Street Name</div>
                     <Input className="Sio-input"/>
                   </div>
                </div>   
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-left-container">
                     <div className="Step-lable">Street Number</div>
                    <Input className="Sio-input"/>
                </div>  
                <div className="Sio-input-left-container">
                    <div className="Sio-lable">Suburb</div>
                    <Search
                        onSearch={value => console.log(value)}
                        style={{ width: 120 }}
                        />
                </div>
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-left-container">
                     <div className="Step-lable">Postcode</div>
                    <Input className="Sio-input"/>
                </div>  
                <div className="Sio-input-left-container">
                    <div className="Sio-lable">State</div>
                    <Select
                        style={{ width: 120 }}>
                        <Option value="jack">VIC</Option>
                        <Option value="lucy">NSW</Option>
                        <Option value="tom">TAS</Option>
                        <Option value="tom">NT</Option>
                        <Option value="tom">WA</Option>
                        <Option value="tom">QLD</Option>
                        <Option value="tom">SA</Option>
                    </Select>
                </div>
            </div>

            <hr className="Sio-divider"/>

             <div className="Sio-title-container">
                <div className="Sio-confirm">Confirm Property Details below</div>
            </div> 

             <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Land   Volume</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
                <div className="Sio-input-container">
                <div className="Sio-input-left-container">  
                   <div className="Sio-lable">Folio</div>
                     <Input className="Sio-input"/>
                   </div>
                </div>   
            </div>

             <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Lot</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
                <div className="Sio-input-container">
                <div className="Sio-input-left-container">  
                   <div className="Sio-lable">Plan</div>
                     <Input className="Sio-input"/>
                   </div>
                </div>   
            </div>
            <hr className="Sio-divider"/>
             <div className="Sio-title-container">
                <div className="Sio-confirm">Payment</div>
            </div> 

            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Price</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
            </div>
            <hr className="Sio-divider"/>
            <div className="Sio-title-container">
                <div className="Sio-confirm">Settlement</div>
            </div>
            
            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Due Date</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
            </div>
            <hr className="Sio-divider"/>
            <div className="Sio-title-container">
                <div className="Sio-confirm">Terms Contract</div>
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                  <div className="Sio-lable"></div>
                  <div className=".Sio-upload">
                  <Upload>
                        <Button>
                        <Icon type="cloud-upload-o" /> Click to Upload
                        </Button>
                    </Upload>
                  </div>
                   </div>  
                </div>
            </div>
            <hr className="Sio-divider"/>
             <div className="Sio-title-container">
                <div className="Sio-confirm">Special Conditions</div>
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                  <div className="Sio-lable"></div>
                  <div className=".Sio-upload">
                  <Upload >
                        <Button>
                        <Icon type="cloud-upload-o" /> Click to Upload
                        </Button>
                    </Upload>
                  </div>
                   </div>  
                </div>
            </div>
            <div className="Sio-button-container">
                <div className="Sio-btn-conatiner">
                    <Button className="Sio-add" type="primary" size="large" onClick={this.cancel}>Cancel</Button>
                    <Button className="Sio-add" type="primary" size="large" onClick={this.confirm}>ADD</Button>
                </div>
            </div>
         </div>   
       </div>  
         );
     }
}
export default Add;