import React from 'react';
import {Icon,Button} from 'antd';
class List extends React.Component {
    renderList(e){
     return <div className="Sio-info">  
                <div className={e.data.classTitle}>{e.data.title}</div>
                <div className="Sio-editor">
                    <span className={e.data.classTitle}>{e.data.editName}</span>
                    <Icon className={e.data.classIcon} type={e.data.editType} style={{color:'rgb(0,135,195)'}}/>
                </div>
                <div className="Sio-editor">
                    <span className={e.data.classTitle}>{e.data.removeName}</span>
                    <Icon className={e.data.classIcon} type={e.data.removeType} style={{color:'red'}} />
                </div>
                <div><Button className={e.data.classBtn}>{e.data.btnName}</Button></div>

            </div>
    }
    render(){
        const self=this.props;
        return(
            <div>
              {this.renderList(self)} 
            </div>
        );
    }
}
export default List;