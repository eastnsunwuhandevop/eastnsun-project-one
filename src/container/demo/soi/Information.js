import React from 'react';
import { Icon,Input,Button,List,Checkbox,Select} from 'antd';
const Option = Select.Option;
const { TextArea } = Input;
class Information extends React.Component {
    renderFormItem(e){
        console.log(e);
        switch(e.type){
           case "input":
              return  <div className={e.classContainer}>
                        <div className={e.classLable}>{e.title}</div>
                        <div><Input className={e.classInput}/></div>
                      </div>
           break;
           case "text":
                return  <div className={e.classLable}>{e.title}</div>     
           break;
           case "warn":
            return <div className={e.classLable}>
                       <Icon type="exclamation-circle" />
                       <span>{e.title}</span>
                    </div>
           break;
           case "divider":
               return <div><hr className={e.classLable}/></div>
           break;
           case "input2":
            return <div className={e.classContainer}>
                      <div className="information-container2">
                        <div className={e.classLable}>{e.title}</div>
                        <Input className={e.classInput}/>
                       </div> 
                   </div>  
           break;
           case "hint":
                return <div className={e.classLable}>{e.title}</div>
           break;
           case "list-title":
                return <div className={e.classContainer}>
                           <div className={e.classLable}>{e.leftTitle}</div>
                           <div className={e.classLable}>{e.centerTitle}</div>
                           <div className={e.classLable}>{e.rightTitle}</div>
                       </div>
           break;
           case "list":
             return    <div className={e.classContainer}>
                            <Checkbox className="information-checkbox"></Checkbox>
                            <div className={e.classLable}>{e.title1}</div>
                            <div className={e.classLable}>{e.title2}</div>
                            <div className={e.classLable}>{e.title3}</div>
                        </div>
           break;
           case "button":
               return <div className={e.classContainer}>
                        <Button type="primary" className={e.classLeft}>{e.leftTitle}</Button>
                        <Button type="primary" className={e.classRight}>{e.centerTitle}</Button>
                        <Button type="primary" className={e.classRight}>{e.rightTitle}</Button>
                     </div>
           break;
           case "button2":
           return <div className={e.classContainer}>
                        <Button className={e.class}>{e.leftTitle}</Button>
                        <Button  className={e.class}>{e.rightTitle}</Button>
                  </div>
           break;
           case "button3":
           return <div className={e.classContainer}>
                        <Button className={e.classButton}>{e.title}</Button>
                  </div>
           break;
           case "select":
               return <div className="information-send-container">
                        <div className={e.classLable}><p>{e.title}</p></div>
                            <Select
                                showSearch
                                style={{ width: 200 }}>
                                <Option value="John Simth">John Simth</Option>
                                <Option value="James Harden">James Harden</Option>
                                <Option value="Chris Paul">Chris Paul</Option>
                            </Select>
                        <div><Button className={e.classButton}>{e.buttonTitle}</Button></div>
                    </div>
           break;
           case "text2":
               return <div className={e.classContainer}>
                           <u className={e.classLable}>{e.title}</u>
                      </div>
           break;
           case "input3":
                return <div className={e.classContainer}>
                            <div className={e.classLable}><p>{e.title}</p></div>
                         <div className={e.classInput}><TextArea rows={4} /></div>
                         <div><Button className={e.classButton}>{e.buttonTitle}</Button></div>
                    </div>
           break;
           case "input4":
                return <div className={e.classContainer}>
                            <div className="information-container2"> 
                            <div className={e.classLable}><p>{e.title}</p></div>
                            <div className={e.classInput}><TextArea rows={4} /></div>
                            </div>  
                    </div>
           break;
           case "title":
                    return <div className="Sio-container">
                                <div className={e.classContainer}>
                                        <div className={e.classLable} onClick={() => this.props.data.func(e)}>{e.title}</div>
                                </div>
                            </div>
           break;
           default:
                return <div className={e.classContainer}>
                                <div className={e.classLable} onClick={() => this.props.data.func(e)}>{e.title}</div>
                                <div className="information-right" onClick={() => this.props.data.func(e)}>
                                    {e.right}
                                    <Icon type={e.type} />
                                </div>
                        </div>
           break;
        }
    }
    renderPage(e){
        var result = [];
            e.data.form_content.map(v => {
              result.push(this.renderFormItem(v));
            });
            return <div>{result}</div>;
         
      }
    render(){
        const self=this.props;
        return(
            <div className="information-content-cotainer">
                {this.renderPage(self)}
            </div>
        );
    }
}
export default Information;