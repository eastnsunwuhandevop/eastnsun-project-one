export const HEADER=[
    {
        header:"My Properties",
        title:"U 2 2Catalina AVE Ashburton",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"Statement of Information-General Section",
        progress:"1",
        total:"3",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"Statement of Information-General Section",
        progress:"2",
        total:"3",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"Statement of Information-General Section",
        subTitle:"Comparable property sales",
        progress:"3",
        total:"3",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"Statement of Information-General Section",
        subTitle:"Comparable property sales",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"WARNING",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        class:"Sio-title",
    },
    {
        header:"My Properties",
        title:"Invite User",
        class:"Sio-title",
    },
];
export const FORM_LIST=e=>[
    {
    func:e.handleSetPayload.bind(e),
    form_content:[ 
        {
            title:"Statement of Information",
            right:"Generate",
            type:"right",
            classContainer:"information-item",
            classLable:"information-lable",
            index:0,
        },
        {
            title:"Code 001 General Sale Authority",
            right:"Generate",
            type:"right",
            classContainer:"information-item",
            classLable:"information-lable",
            index:1,
        },
        {
            title:"Code 002 Exclusice Sale Authority",
            right:"Generate",
            type:"right",
            classContainer:"information-item",
            classLable:"information-lable",
            index:2,
        },
        {
            title:"Code 003 Exclusice Action Authority",
            right:"Generate",
            type:"right",
            classContainer:"information-item",
            classLable:"information-lable",
            index:3,
        },
        {
            title:"Code 118 Contract of Sale of Real Estate",
            right:"Generate",
            type:"right",
            classContainer:"information-item-bottom",
            classLable:"information-lable",
            index:5,
        }
     ],
    },
    {
        func:e.handleSetPayload.bind(e),
        form_content:[ 
            {
                title:"Choose following statement of information template below:",
                classContainer:"information-step-one",
                classLable:"information-lable_title",
                type:"title",
                index:0,
            },
            {
                title:"Internet advertising for single residential property",
                classContainer:"information-step-one",
                classLable:"information-lable",
                type:"title",
                index:0,
            },
            {
                title:"Sigle residential property located in the Melbourne metropolitian area",
                classContainer:"information-step-one",
                classLable:"information-lable",
                type:"title",
                index:0,
            },
            {
                title:"Multiple residential properties Located in the Melbourne metropolitan area",
                classContainer:"information-step-one-bottom",
                classLable:"information-lable",
                type:"title",
                index:0,
            },
         ],
        },
        {
            form_content:[ 
                {
                    title:"Property offered for sal",
                    type:"input",
                    classContainer:"information-step_two",
                    classLable:"information-lable",
                },
                {
                    title:"Indicative Selling Price",
                    type:"input",
                    classContainer:"information-step_two",
                    classLable:"information-lable",
                },
             ],
            },
            {
                form_content:[
                     {
                        title:"",
                        type:"input",
                        
                    },
                    {
                        title:"Indicative Selling Price",
                        type:"input",
                        classContainer:"information-step_Three",
                        classLable:"information-lable",
                    },
                 ],
                },
            {
                form_content:[
                     {
                        title:"",
                        type:"input",
                        classInput:"information-input",
                        classContainer:"information-input-container",
                     },
                     {
                        title:"Indicative Selling Price",
                        type:"input",
                        classContainer:"information-step_Three",
                        classLable:"information-lable",

                     },
                     {
                        title:"Indicative Selling Price falls above the comparable price  Please update",
                        type:"warn",
                        classLable:"Information-warn",
                     },
                     {
                         type:"divider",
                         classLable:"Step-divider",
                     },
                     {
                         title:"Median Sale Price",
                         type:"text",
                         classLable:"information-title-two",
                     },
                     {
                        title:"Median Sale Price",
                        type:"input2",
                        classContainer:"Sio-price-container",
                        classLable:"Sio-lable",
                        classInput:"Sio-input",
                    },
                    {
                        title:"House Number",
                        type:"input2",
                        classContainer:"Sio-price-container",
                        classLable:"Sio-lable",
                        classInput:"Sio-input",
                    },
                    {
                        title:"Periad",
                        type:"input2",
                        classContainer:"Sio-price-container",
                        classLable:"Sio-lable",
                        classInput:"Sio-input",
                    },
                    {
                        title:"Suburb",
                        type:"input2",
                        classContainer:"Sio-price-container",
                        classLable:"Sio-lable",
                        classInput:"Sio-input",
                    },
                    {
                        title:"Source",
                        type:"input2",
                        classContainer:"Sio-price-container",
                        classLable:"Sio-lable",
                        classInput:"Sio-input",
                    },
                    {
                        type:"divider",
                        classLable:"Step-divider",
                    },
                    {
                        title:"Comparable property sales",
                        type:"text",
                        classLable:"information-title-two",
                    },
                     {
                        title:"These are the three properties sold within two/five kilometres of the property for sale in the last six/eighteenmonths that the easte agent or agent’s representive considers to be most comparable to the property for sale.",
                        type:"hint",
                        classLable:"information-hint",
                    },
                    {
                        leftTitle:"Address of Comparable Property",
                        centerTitle:"Price",
                        rightTitle:"Date of Sale",
                        type:"list-title",
                        classContainer:"information-list",
                        classLable:"information-list-title",
                    },
                    {
                        title1:"118ALinacre Road,Ashburton",
                        title2:"$900,580,000",
                        title3:"17March2016",
                        type:"list",
                        classContainer:"information-list-item-container1",
                        classLable:"information-list-item",
                    },
                    {
                        title1:"118ALinacre Road,Ashburton",
                        title2:"$900,580,000",
                        title3:"17March2016",
                        type:"list",
                        classContainer:"information-list-item-container2",
                        classLable:"information-list-item",
                    },
                    {
                        title1:"118ALinacre Road,Ashburton",
                        title2:"$900,580,000",
                        title3:"17March2016",
                        type:"list",
                        classContainer:"information-list-item-container1",
                        classLable:"information-list-item",
                    },
                    {
                        title1:"118ALinacre Road,Ashburton",
                        title2:"$900,580,000",
                        title3:"17March2016",
                        type:"list",
                        classContainer:"information-list-item-container2",
                        classLable:"information-list-item",
                    },
                    {
                        leftTitle:"Complete selection foe review",
                        centerTitle:"Generate PDF",
                        rightTitle:"Generate QR Code",
                        type:"button",
                        classLeft:"information-button-left",
                        classRight:"information-button-right",
                        classContainer:"information-btn-container",
                    },
                ],
        }, 
        {
            form_content:[
                 {
                    title:"The statement of information will be used as an unchangeable",
                    type:"text",
                    classLable:"information-warning-hint-one",
                    
                },
                {
                    title:"information.Please double check the  information is correct or not.",
                    type:"text",
                    classLable:"information-warning-hint-two",
                },
                {
                    leftTitle:"CANCEL",
                    rightTitle:"CONFIRM",
                    type:"button2",
                    class:"information-button2",
                    classContainer:"information-btn-container",
                },
             ],
            },
            {
                form_content:[
                     {
                        title:"Assign it to user for action",
                        type:"select",
                        buttonTitle:"Invite User",
                        classLable:"information-lable",
                        classButton:"information-button2",
                        classContainer:"information-send-container",
                    },
                    {
                        title:"Not available?Invite User",
                        type:"text2",
                        classLable:"information-text",
                        classContainer:"information-text-container",
                    },
                    {
                        title:"Message to he User",
                        type:"input3",
                        buttonTitle:"Assign",
                        classLable:"information-lable",
                        classInput:"information-textArea",
                        classButton:"information-button2",
                        classContainer:"information-send-container2",
                    },
                 ],
                },
                {
                    form_content:[
                         {
                            title:"Name",
                            type:"input2",
                            classLable:"information-lable",
                            classContainer:"information-invite-container",
                            classInput:"information-invite-input",
                        },
                        {
                            title:"Email",
                            type:"input2",
                            classLable:"information-lable",
                            classContainer:"information-invite-container",
                            classInput:"information-invite-input",
                        },
                        {
                            title:"Phone",
                            type:"input2",
                            classLable:"information-lable",
                            classContainer:"information-invite-container",
                            classInput:"information-invite-input",
                        },
                        {
                            title:"Notes",
                            type:"input4",
                            classLable:"information-lable",
                            classInput:"information-textArea",
                            classContainer:"information-note-container",
                        },
                        {
                            title:"Send Invitation",
                            type:"button3",
                            classLable:"information-lable",
                            classButton:"information-button2",
                            classContainer:"information-btn3-container",
                        },
                     ],
                    },
];