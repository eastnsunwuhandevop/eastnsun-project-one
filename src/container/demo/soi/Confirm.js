import React from 'react';
import {Input,Icon,Button} from 'antd';
class Confirm extends React.Component {
    render(){
        return(
        <div className="Sio-info2-container">    
            <div className="Sio-title-container">
                <div className="Sio-modify">Property Infomation</div>
                <div><Icon type="form" style={{ fontSize: 20, color: '#08c' }}/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Land   Volume</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Folio</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Lot</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Plan</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <hr className="Sio-divider"/>
            <div className="Sio-title-container">
                <div className="Sio-modify">Payment</div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Price</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <hr className="Sio-divider"/>
            <div className="Sio-title-container">
                <div className="Sio-modify">Payment</div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Due Date</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <hr className="Sio-divider"/>
                <div className="Sio-title2-container">
                    <div className="Sio-modify">Terms Contract</div>
                    <div className=".Sio-upload"><Input/></div>
                    <Icon className="Sio-upload-icon" type="cloud-upload-o" />
                </div>
                <div className="Sio-title2-container">
                    <div className="Sio-modify">Special Conditions</div>
                    <div className=".Sio-upload"><Input/></div>
                    <Icon className="Sio-upload-icon" type="cloud-upload-o" />
                </div>
            <Button className="Sio-add" type="primary" size="large">Confirm</Button>
        </div>    
        );
    }
}
export default Confirm;