import React from 'react';
import { Input,Button} from 'antd';
const Search = Input.Search;
class Invite extends React.Component {
    send=()=>{
        this.props.history.push('Item')
    }
    render(){
        return(
            <div className="Sio-container">
             <div className="Sio-property-container">  
                    <div className="Sio-detail-title">Property</div>
                </div>    
              <div className="Sio-invite-container">
                <div className="Sio-invite-detail-container">
                   <div className="Sio-search-container Sio-detail-button">
                      <Search className="Sio-search2"enterButton="Search" />
                   </div>
                   <div className="Sio-info3">  
                        <div className="Sio-name">Cricy</div>
                        <div className="Sio-progress">CRICY@sdhajdg.com</div>
                        <div onClick={this.send}><Button className="Sio-button">Send</Button></div>
                    </div>
                </div>
                <div className="Sio-invite-detail-container">
                      <div className="Sio-invite-title Sio-detail-button">Invite Seller</div>
                      <div className="Sio-invite-input-container">
                       <div className="Sio-name"> Email</div>
                           <Input className="Sio-search3"/>
                      </div>
                      <div onClick={this.send}><Button className="Sio-invite-button">Send</Button></div>
                </div>
              </div>
            </div>
        );
    }
}
export default Invite;