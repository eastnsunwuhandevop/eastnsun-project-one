import React from 'react';
import './Soi.css';
import {Icon} from 'antd';
class Soi extends React.Component {
    add=()=>{
        this.props.history.push('Add')
    }
     render(){
         return(
            <div className="Sio-container">
                <div className="Sio-information-container">
                  <div className="Sio-hint">Please add property information</div>
                  <div className="Sio-add-icon" onClick={this.add}>
                      <Icon type="plus-circle-o" style={{ fontSize: 40, color: '#08c' }} />
                  </div>
                </div>
            </div>
         );
     }
}
export default Soi;