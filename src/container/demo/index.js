import React, {Component} from 'react'

import Navbar from '../../component/index/navbar/Navbar'
import Footer from '../../component/index/footer/Footer'
import {INDEX_PAGE} from '../../sources/source'

class DemoIndex extends Component{
    
    renderIndex(){
        return <div>
            <Navbar/>
           
            <Footer/>
            </div>
    }
    
    render(){
        return <div>
            demo
        </div>
    }
}

export default DemoIndex