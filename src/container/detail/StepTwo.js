import React, { Component } from 'react';
import './Detail.css';
import { DatePicker,Input,Select,Form} from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

class StepTwo extends Component {
	constructor(props) {
		super(props);
		this.state = {
		selectValue:"",
		cardNumber:"",
		accountNumber:"",
		};
	}
	handleChange=(value)=>{
		this.setState({
			selectValue:value,
		});
	}
	onChangeCardNumber=(e)=>{
		this.setState({
			cardNumber:e.target.value,
	});
}
	onChangeCardNumber=(e)=>{
		this.setState({
			accountNumber:e.target.value,
		});
 }
 onChangeDate=(date,dateString)=>{
	console.log(date, dateString);
 }
	render(){
		return(
			 <div className="Step">
			     <div className="Step-information-container">
					  <div className="Step-information">
							<div className="Step-lable">Payment Type</div>
							<FormItem
                              >
								<Select
									showSearch
									style={{ width: 200 }}
									optionFilterProp="children"
									onChange={this.handleChange}
									filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								>
									<Option value="Credit Card Number">Credit Card Number</Option>
									<Option value="PayPal Account Number">PayPal Account Number</Option>
								</Select>
							</FormItem>	
	          </div>
	             </div>
	             <div className="Step-information-container">     
	                   <div className="Step-information2">
										 <div className="Step-lable">Credit Card Number</div>
										 <FormItem
											hasFeedback
											>	 
										 <Input className="Step-input2" onChange={this.onChangeCardNumber}/>
										 </FormItem>
									</div>
	             </div> 
	             <div className="Step-information-container">       
					   <div className="Step-information2">
									<div className="Step-lable">PayPal Account Number</div>
									<FormItem
											hasFeedback
											>	 
										 <Input className="Step-input2" onChange={this.onChangeAccountNumber}/>
									</FormItem>		 
						 </div>
			     </div> 
			     <div className="Step-information-container">		   
					   <div className="Step-information2">
									 <div className="Step-lable">Expiry Date</div>
								<FormItem
										>	  
										<DatePicker size="large" onChange={this.onChangeDate} className="Step-input2" />
								</FormItem>		
						 </div>
		         </div>			   
				  </div> 
			);
	}
}
export default StepTwo;