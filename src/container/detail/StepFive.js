import React, { Component } from 'react';
import './Detail.css';
import {Icon,Checkbox,Switch} from 'antd';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import StepFour from './StepFour';
class StepFive extends Component {
	render(){
		return(
			<div className="Step5">
			   <div className="Step-information-container">  	
	                <div className="Step-information">
	                    <p>Business</p>
						<Icon type="form" style={{ fontSize: 20, color: '#08c' }} className="Step-modify"/>
	                </div>
	           </div>
	           <StepOne/>
	            <hr className="Step-divider"/>
	            <div className="Step-information-container">  	
	                <div className="Step-information">
	                    <p>Address</p>
						<Icon type="form" style={{ fontSize: 20, color: '#08c' }} className="Step-modify"/>
	                </div>
	           </div>
	           <StepTwo/>
	           <hr className="Step-divider"/>
	           <div className="Step-information-container">  	
	                <div className="Step-information">
	                    <p>Postal Address</p>
						<Icon type="form" style={{ fontSize: 20, color: '#08c' }} className="Step-modify"/>
	                </div>
	           </div>
	           <StepThree/>
	            <hr className="Step-divider"/>
	           <div className="Step-information-container">  	
	                <div className="Step-information">
	                    <p>Postal Administrator</p>
						<Icon type="form" style={{ fontSize: 20, color: '#08c' }} className="Step-modify"/>
	                </div>
	           </div>
	           <StepFour/>
	           <div className="Step-checkbox-container">        
                   <div className="Step-checkbox-accept">
				   <Checkbox className="Step-checkbox">I accept Eastnsun's Terms of Use</Checkbox> </div>
	           </div> 
	           <div className="Step-switch-container">        
	                   <div className="Step-switch-robot">
					   <Switch /><span className="span"> I am not a robot</span></div>
	           </div>
			</div>
			);
	}
}
export default StepFive;