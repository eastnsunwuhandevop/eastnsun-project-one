import React, { Component } from 'react';
import {DatePicker,Input,Form} from 'antd';
const FormItem = Form.Item;
class StepFour extends Component {
	render(){
	  return(
             <div className="Step">
                <div className="Step-information-container">
                <FormItem
                        >	 
                  <Input placeholder="First Name" onChange={this.onChangeFirstName} className="Step-FirstName-input"/>
                </FormItem>  
                <FormItem
                        >
                  <Input placeholder="Last Name" onChange={this.onChangeLastName} className="Step-LastName-input"/>
                </FormItem>  
                </div>
                <div className="Step-information-container">
                <FormItem
                        > 
                   <Input placeholder="Post Code" onChange={this.onChangePostCode} className="Step-PostCode-input"/>
                </FormItem> 
                <FormItem
                        >  
                   <Input placeholder="Phone" onChange={this.onChangePhone} className="Step-Phone-input"/>
                </FormItem>   
                </div>
                <div className="Step-information-container">
                    <div className="Step-information2">
                        <div className="Step-lable">DOB</div>
                        <FormItem
                             >   
                            <DatePicker size="large" onChange={this.onChangeDate} className="Step-input2" />
                        </FormItem>
                    </div> 
                </div> 
                <div className="Step-information-container">        
                    <div className="Step-information2">
                        <div className="Step-lable">Email</div>
                            <FormItem
                                >   
                              <Input className="Step-input2" onChange={this.onChangeEmail}/>
                             </FormItem>
                        </div>   
                </div> 
                <div className="Step-information-container">         
                        <div className="Step-information2">
                           <div className="Step-lable">Licsence No</div>
                              <FormItem
                                >   
                                <Input className="Step-input2" onChange={this.onChangeLicsenceNo}/>
                               </FormItem>
                           </div> 
                </div> 
                <div className="Step-information-container">        
                     <div className="Step-information2">
                          <div className="Step-lable">Certificate</div>
                          <FormItem
                                > 
                            <Input className="Step-input2" onChange={this.onChangePassportNo}/>
                           </FormItem>
                     </div> 
                </div> 
	</div>
			);
	}
}
export default StepFour;