import React, { Component } from 'react';
import './Detail.css';
import {Form, Input} from 'antd';
const FormItem = Form.Item; 
class StepOne extends Component {
	constructor(props) {
		super(props);
		this.state = {
		companyName:"",
		legalEntityNo:"",
		abn:"",
		acn:"",
		phone:"",
		fax:"",
		email:"",
		};
	}
	onChangeCompanyName=(e)=>{
       this.setState({
		   companyName:e.target.value,
	   });
	}
	onChangeLegalEntityNo=(e)=>{
		this.setState({
			legalEntityNo:e.target.value,
		});
	 }
	 onChangeABN=(e)=>{
		this.setState({
			abn:e.target.value,
		});
	 }
	 onChangeACN=(e)=>{
		this.setState({
			acn:e.target.value,
		});
	 }
	 onChangePhone=(e)=>{
		this.setState({
			phone:e.target.value,
		});
	 }
	 onChangeFax=(e)=>{
		this.setState({
			fax:e.target.value,
		});
	 }
	 onChangeEmail=(e)=>{
		this.setState({
			email:e.target.value,
		});
	 }
   render(){
   	  return(
   	  	   <div className="Step">
	   	  	    <div className="Step-information-container">  
				   <div className="Step-information">
						<div className="Step-lable">Company Name</div>
						<FormItem
							hasFeedback
							>
						 <Input className="Step-input2" onChange={this.onChangeCompanyName}/>
						</FormItem>
				   </div>
				</div>
				<div className="Step-information-container"> 
				   <div className="Step-information2">
						<div className="Step-lable">Legal Entity No</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangeLegalEntiyNo}/>
						</FormItem>
				   </div>
				  
				</div>   
				<div className="Step-information-container"> 
				   <div className="Step-information2">
						<div className="Step-lable">ABN</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangeABN}/>
						</FormItem>
				   </div>
				</div> 
				<div className="Step-information-container">   
				   <div className="Step-information2">
						<div className="Step-lable">ACN</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangeACN}/>
						</FormItem>
				   </div>
				</div>
				<div className="Step-information-container">   
				   <div className="Step-information2">
						<div className="Step-lable">Phone</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangePhone}/>
						</FormItem>
				   </div>
				</div> 
				<div className="Step-information-container">   
				   <div className="Step-information2">
						<div className="Step-lable">FAX</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangeCompanyFax}/>
						</FormItem>
				   </div>
				</div>   
				<div className="Step-information-container">  
				   <div className="Step-information2">
						<div className="Step-lable">Email</div>
						<FormItem
							hasFeedback
							>
						<Input className="Step-input2" onChange={this.onChangeEmail}/>
						</FormItem>
				   </div>
				</div>   
		  </div>
   	  	);
   }
}
export default StepOne;