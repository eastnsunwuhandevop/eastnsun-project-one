import React, { Component } from 'react';
import './Detail.css';
import { Input,Form} from 'antd';
const Search = Input.Search;
const FormItem = Form.Item;
class StepThree extends Component {
	constructor(props) {
		super(props);
		this.state = {
			businessAddress:"",
			postalAddress:"",
			familyAddress:"",
		};
	}
	onSearchBusinessAddress=()=>{
		this.setState({
		});
	}
	onSearchPostalAddress=()=>{
		this.setState({
		});
	}
	onSearchFamilyAddress=()=>{
		this.setState({
		});
	}
    render(){
    	return(
    <div className="Step">
    	<div className="Step-information-container">	
             <div className="Step-information">
			      <div className="Step-lable">Business Address</div>
				  <FormItem
						>	 		 
						<Search onSearch={this.onSearchBusinessAddress}
							className="Step-input2"/>
				   </FormItem>			
			  </div>
	     </div>
	     <div className="Step-information-container">		  
			   <div className="Step-information2">
					<div className="Step-lable">Postal Address</div>
					<FormItem
							>	 	
						<Search onSearch={this.onSearchPostalAddress}
							className="Step-input2"/>
				    </FormItem>		
			    </div>
	      </div>
	       <div className="Step-information-container">		    
			     <div className="Step-information2">
					<div className="Step-lable">Family Address</div>
					<FormItem
							>	
						<Search onSearch={this.onSearchFamilyAddress}
					className="Step-input2"/>
					</FormItem>
			     </div>
			 </div>
		  </div>
    		);
    }
}
export default StepThree;