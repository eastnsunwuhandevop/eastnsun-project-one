import React from 'react';
import './Detail.css';
import StepOne from './StepOne';
import StepTwo from './StepTwo';
import StepThree from './StepThree';
import StepFour from './StepFour';
import StepFive from './StepFive';
import { Button,Icon} from 'antd';
class Detail extends React.Component  {
	constructor(props) {
	  super(props);
	
	  this.state = {
	  	index:1,
	  	type:"dashed",
	  };
	}
	//上一步
    previous=()=>{
    	if(this.state.index>1){
       	   this.setState({index:this.state.index-1
    	});
       }
	}
	//下一步
    next=()=>{
    	if(this.state.index<=4){
       	   this.setState({index:this.state.index + 1
    	});
    	console.log(this.state.index);
    }
}
   //设置头部进度
    setStep(){
    	var tools=[];
    	for (var i = 1 ; i <=5; i++) {
          	if(i===5){
          	  if(this.state.index===5){
          	  	tools.push(<div className="Step-number"><div className="Step-circle-one">{i}</div></div>);
          	  }	else{
          	  	tools.push(<div className="Step-number"><div className="Step-circle-two">{i}</div></div>);
          	  }
    		}else{
    			switch(i){
    				case 1:
	    				if(this.state.index===1){
	    					 tools.push(<div className="Step-number"><div className="Step-circle-one">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}else{
	    					 tools.push(<div className="Step-number"><div className="Step-circle-one"><Icon type="check"/></div>
				            <hr className="Step-divider-blue"/></div>);
	    				}
    				break;
    				case 2:
    				   if(this.state.index===2){
	    					 tools.push(<div className="Step-number"><div className="Step-circle-one">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}else if(this.state.index>2){
                              tools.push(<div className="Step-number"><div className="Step-circle-one"><Icon type="check"/></div>
				            <hr className="Step-divider-blue"/></div>);
	    				}else{
	    					 tools.push(<div className="Step-number"><div className="Step-circle-two">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}
    				break;
    				case 3:
    				   if(this.state.index===3){
	    					 tools.push(<div className="Step-number"><div className="Step-circle-one">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}else if(this.state.index>3){
                             tools.push(<div className="Step-number"><div className="Step-circle-one"><Icon type="check"/></div>
				            <hr className="Step-divider-blue"/></div>);
	    				}else{
	    					 tools.push(<div className="Step-number"><div className="Step-circle-two">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}
    				break;
    				case 4:
    				   if(this.state.index===4){
	    					 tools.push(<div className="Step-number"><div className="Step-circle-one">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}else if(this.state.index>4){
                            tools.push(<div className="Step-number"><div className="Step-circle-one"><Icon type="check"/></div>
				            <hr className="Step-divider-blue"/></div>);
	    				}else{
	    					 tools.push(<div className="Step-number"><div className="Step-circle-two">{i}</div>
				            <hr className="Step-divider-gray"/></div>);
	    				}
						break;
						default:
						break;
    			}
    		   
			  }	
    	}
    	return tools;
    }
    //设置中间的内容
    setContent(){
    	switch(this.state.index){
    		case 1:
    		   return <div><StepOne/></div>
    		case 2:
    		   return <div><StepTwo/></div>
    		case 3:
    		   return <div><StepThree/></div>
    		case 4:
    		   return <div><StepFour/></div>
    		case 5:
    		   return <div><StepFive/></div>
				default:
				break;
    	}
    }
    //设置底部按钮
    setButton(){
        if(this.state.index===5){
           return <Button type="primary" className="Step-confirm">Confirm</Button>
        }else{
          if(this.state.index>1){
            return <div className="Step-arrow">
						<Button type="primary" onClick={this.previous}>
							<Icon type="left" />Previous
						</Button>
						<Button type="primary" onClick={this.next}>
							Next<Icon type="right" />
						</Button>
                   </div>
    	  }else{
    	  	return <div className="Step-arrow">
						<Button type="dashed" onClick={this.previous}>
							<Icon type="left" />Previous
						</Button>
						<Button type="primary" onClick={this.next}>
							Next<Icon type="right" />
						</Button>
                   </div>
    	  }
        }
	}
	//设置标题
	setTitle(){
        switch(this.state.index){
			case 1:
			  return  <div className="Step-hint">Step {this.state.index} of 5:Enter Your Business Information</div>
			case 2:
			  return  <div className="Step-hint">Step {this.state.index} of 5:Enter Your Business Credit Card/PayPal</div>
			case 3:
			  return <div className="Step-hint">Step {this.state.index} of 5:Enter Your Business Credit Card/PayPal</div>
			case 4:
			  return <div className="Step-hint">Step {this.state.index} of 5:Enter Administrator Information</div>
			case 5:
			  return <div className="Step-hint">Step {this.state.index} of 5:Confirm of Details</div>
			default:
			break;
		}
	}

	render(){
		return(
		  <div className="Step-body">	
			<div className="Step-container">
			   <div className="Step-text">Complete you Details</div>
			<div className="Step-number-container">   
			   <div className="Step-number">
			     {this.setStep()}
			   </div>
			 </div>  
			      {this.setTitle()}
			      {this.setContent()}
             <div className="step-arrow-container">    
			    {this.setButton()}
			</div>   
			</div>
		  </div>	
		);
	}
}
export default Detail;