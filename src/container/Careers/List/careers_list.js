import React, { Component } from "react";
import './careers_list.css';
class Careers_list extends Component{
    renderRedirect = (e) =>{
        console.log(this.props)
        this.props.props.history.push(e)
    }
    render(){
        return(
            <div className="careers-list-container-box">
                <div className="careers-list-careers-container-box"> 
                     <h1 className="careers-list-open-box">
                         Open Roles
                     </h1>
                     <div className="careers-list-nav-box">
                        <div className="careers-list-role-box">
                            Role                           
                        </div>
                        <div className="careers-list-role-box">
                            Team 
                        </div>
                        <div className="careers-list-role-box"> 
                           City
                        </div>
                    </div>
                    <div className="careers-list-list-01" onClick={()=>this.renderRedirect('/career/python')}>
                        <div className="careers-list-python-box">
                            Python Developer                           
                        </div>
                        <div className="careers-list-python-box">
                            Engineering
                        </div>
                        <div className="careers-list-python-box"> 
                           Wuhan,CH
                        </div>
                    </div>
                    <div className="careers-list-list-02" onClick={()=>this.renderRedirect('/career/go')}>
                        <div className="careers-list-golang-box">
                            GOlang Developer                           
                        </div>
                        <div className="careers-list-golang-box">
                            Engineering
                        </div>
                        <div className="careers-list-golang-box"> 
                           Wuhan,CH
                        </div>
                    </div>
                </div>
            <div className="careers-list-careers-chiness-container-box"> 
                 <h1 className="careers-list-open-chiness-box">
                     正在招聘
                 </h1>
                <div className="careers-list-nav-chiness-box">
                    <div className="careers-list-role-chiness-box">
                        岗位                           
                    </div>
                    <div className="careers-list-role-chiness-box">
                        部门 
                    </div>
                    <div className="careers-list-role-chiness-box"> 
                       城市
                    </div>
                </div>
                <div className="careers-list-list-chiness-01" onClick={()=>this.renderRedirect('/zh/career/python')}>
                    <div className="careers-list-python-chiness-box">
                        Python 开发员                           
                    </div>
                    <div className="careers-list-python-chiness-box">
                        研发部
                    </div>
                    <div className="careers-list-python-chiness-box"> 
                       中国武汉
                    </div>
                </div>
                <div className="careers-list-list-chiness-02" onClick={()=>this.renderRedirect('/zh/career/go')}>
                    <div className="careers-list-golang-chiness-box">
                        GOlang 开发员                           
                    </div>
                    <div className="careers-list-golang-chiness-box">
                        研发部
                    </div>
                    <div className="careers-list-golang-chiness-box"> 
                       中国武汉
                    </div>
                </div>
            </div>
        </div>


        );
    }
}
export default Careers_list;