import React, { Component } from "react";
import Careers_list from "./List/careers_list";
import { INDEX_PAGE } from "../../sources/source";
import Footer from '../../component/index/footer/Footer'
import Navbar from '../../component/index/navbar/Navbar'
class CareersIndex extends Component {
  switchLang(){
    const lan = this.props.location.pathname.substr(0, 3)
    if(lan === '/zh')
    {  
        const newAddress = this.props.location.pathname.slice(3, this.props.location.pathname.length);
        this.props.history.push(newAddress)
    } else {
        this.props.history.push('/zh' + this.props.location.pathname)
    }
}
  renderIndex(e) {
    return (
      <div>
        <Navbar source={e.header} switchLang={this.switchLang.bind(this)}/>
        <Careers_list props={this.props}/>
        <Footer  source={e.footer} switchLang={this.switchLang.bind(this)}/>
      </div>
    );
  }
  render() {
    const lan = this.props.location.pathname.substr(0, 3)
    const language = lan === '/zh' ?  INDEX_PAGE.cn : INDEX_PAGE.en
    return <div>{this.renderIndex(language)}</div>;
  }
}

export default CareersIndex;
