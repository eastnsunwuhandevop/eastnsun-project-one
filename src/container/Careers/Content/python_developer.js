import React, { Component } from "react";
import './python_developer.css';
class Python_developer extends Component{
    render(){
        return(
            <div className="container-box">
                <div className="python-container-box">
                    <div className="python-box">
                        Python Developer
                    </div>
                    <div className="job-box">
                        Engineering in Wuhan,CH
                    </div>
                    <div className="about-box">
                        <h1 className="about-head">
                            About the Company and Role
                        </h1>
                        <p className="about-content">
                        We are looking for a Python Developer to help develop a great platform. This position will be based at our Wuhan office.
                        </p>
                    </div>
                    <div className="requirement-box">
                        <h1 className="requirement-head">
                            Requirements
                        </h1>
                        <ul className="requirement-list">
                            <li className="list_one">
                                <div className="list-content-box">
                                3+ years of commercial experience in Python development
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                Linux development experience
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                Version control experience (i.e. Git)
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                RDBMS and SQL experience
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                Testing framework experience
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                Experience with Microservice-based architecture (i.e. Kubernetes, Docker, etc.)
                                </div>
                            </li>
                            <li className="list_one">
                                <div className="list-content-box">
                                Experience working within an Agile and/or Kanban environment
                                </div>
                            </li>
                        </ul>
                        <div className="how-box">
                             <h1 className="how-head">
                                How to Apply
                            </h1>
                            <p className="how-content">
                                If you are after a great career opportunity with an excellent organisation, then please apply now. Please send your application to &nbsp;  
                                 <a className="link-box" href="hr@eastnsun.com">
                                    hr@eastnsun.com
                                </a>
                                , thanks.
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
export default Python_developer;