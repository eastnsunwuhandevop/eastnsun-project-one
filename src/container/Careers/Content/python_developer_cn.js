import React, { Component } from "react";
import './python_developer.css';
class Python_developer_cn extends Component{
    render(){
        return(
            <div className="container-box">
                <div className="python-chiness-container-box">
                    <div className="python-chiness-box">
                        Python 开发员
                    </div>
                    <div className="job-chiness-box">
                        中国武汉研发部
                    </div>
                    <div className="about-chiness-box">
                        <h1 className="about-chiness-head">
                            关于职位
                        </h1>
                        <p className="about-chiness-content">
                        我们目前正在开发一个需要 Python 语言的项目，这个工作岗位是在湖北省武汉市。
                        </p>
                    </div>
                    <div className="requirement-chiness-box">
                        <h1 className="requirement-chiness-head">
                            具体要求
                        </h1>
                        <ul className="requirement-chiness-list">
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                熟练运用Python至少要有3年经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有 Linux 开发经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有视觉控制经验（Git）
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有关系型数据库管理系统（RDBMS）和数据库（SQL）的经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过测试框架经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过在 Microservice-based 架构（例如：Kubernetes, Docker）经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过在 Agile 或者 Kanban 环境开发的经验
                                </div>
                            </li>
                        </ul>
                        <div className="how-chiness-box">
                             <h1 className="how-chiness-head">
                                如何申请
                            </h1>
                            <p className="how-chiness-content">
                                如果您正在寻找一份极具挑战力的工作，欢迎您向我们公司提出申请。求职邮件发到 &nbsp;  
                                 <a className="link-chiness-box" href="hr@eastnsun.com">
                                    hr@eastnsun.com
                                </a>
                                ，谢谢。
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
export default Python_developer_cn;