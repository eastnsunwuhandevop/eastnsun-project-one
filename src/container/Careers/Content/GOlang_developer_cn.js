import React, { Component } from "react";
import './GOlang_developer.css';
class GOlang_developer_cn extends Component{
    render(){
        return(
            <div className="container-box">
                <div className="golang-chiness-box">
                    <div className="go-chiness-box">
                        GO语言开发员
                    </div>
                    <div className="job-chiness-box">
                        中国武汉研发部
                    </div>
                    <div className="about-chiness-box">
                        <h1 className="about-chiness-head">
                            关于该职位
                        </h1>
                        <p className="about-chiness-content">
                            我们目前正在开发一个需要Go 语言的项目, 这个工作岗位是在湖北省武汉市
                        </p>
                    </div>
                    <div className="requirement-chiness-box">
                        <h1 className="requirement-chiness-head">
                            具体要求
                        </h1>
                        <p className="requirement-chiness-content">
                        我们希望你是一位聪明，品貌兼优的IT从业人员。你能够清晰地表达你的观点，能用你灵敏的思维去写你想要表达的代码，并且你还对学习新的语言和框架抱有极大的热情。我们希望你已经有一个 GitHub账号并且在上面有一些你做的很棒的项目。
                        </p>
                        <ul className="requirement-chiness-list">
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                熟练运用Go/Golang至少要有3年经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有出色的 JavaScript 开发技能
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有视觉控制经验（Git）
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                拥有关系型数据库管理系统（RDBMS）和数据库（SQL）的经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过测试框架经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过在 Microservice-based 架构（例如：Kubernetes, Docker）经验
                                </div>
                            </li>
                            <li className="list-chiness_one">
                                <div className="list-chiness-content-box">
                                有过在 Agile 或者 Kanban 环境开发的经验
                                </div>
                            </li>
                        </ul>
                        <div className="how-chiness-box">
                             <h1 className="how-chiness-head">
                                如何申请
                            </h1>
                            <p className="how-chiness-content">
                            如果您正在寻找一份极具挑战力的工作，欢迎您向我们公司提出申请。求职邮件发到 &nbsp;  
                                 <a className="link-chiness-box" href="hr@eastnsun.com">
                                    hr@eastnsun.com
                                </a>
                                ，谢谢。
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        );
    }
}
export default GOlang_developer_cn;