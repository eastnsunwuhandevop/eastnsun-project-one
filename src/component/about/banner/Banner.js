import React, { Component } from "react";
import "./AboutBanner.css";
import { OUR, ABOUT } from "../../../sources/source"

class AboutBanner extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }
  render() {
    
    const ourteam = this.props.lang ?  OUR.en : OUR.cn
    const about = this.props.lang ?  ABOUT.en : ABOUT.cn
    const banner = this.props.lang ?  OUR.head.en : OUR.head.cn
    return (
      <div className="about">
        <div className="bannerContent">
          <div className="aboutBanner">
            <p className="bannerMainTitle">{banner.title}</p>
            <p className="bannerSubTitle">{banner.subTitle}</p>
          </div>
        </div>

        <div className="textContent">
          <div className="aboutCompany">
            <p className="mainTitle">{about.title}</p>
            <p className="subTitle">{about.storyText.title}</p>
            <p className="text">{about.storyText.text}</p>
            <p className="subTitle">{about.visionText.title}</p>
            <p className="text">{about.visionText.text}</p>
            <p className="subTitle">{about.missionText.title}</p>
            <p className="text">{about.missionText.text}</p>
          </div>
        </div>

        <div className="teamContent">
          <div className="aboutTeam">
            <div className="aboutTeamTitle">
              <p className="mainTitle">{ourteam.title}</p>
              <p className="subTitle">{ourteam.subTitle}</p>
            </div>

            <div className="pictureList">
                {ourteam.list.map((v, index)=>{
                    return  (
                        <div className="teamPicture">
                            <div>
                            <img className="img" src={v.imagePath} alt=""/>
                            </div>
                            <p className="name">{v.name}</p>
                            <p className="companyTitle">{v.title}</p>
                        </div>
                        
                    )
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default AboutBanner;
