import React from 'react';
import './Identify.css';

class Identify extends React.Component {
	click(){
		this.props.history.push('Profession')
	}
	render(){
		return(
            <div>
                <div className="Identify-container">
                    <div>
                        <p className="Identify-title">Choose your identify</p>
                    </div>
                    <div className="Identify-Btn">
                        <div className="Identify-left" >Buyer</div>
                        <div className="Identify-right">Seller</div>
                        <div className="Identify-left">Company</div>
                        <div className="Identify-right Identify-small">Government Agencies and Industry Bodies</div>
                    </div>
                </div>
             </div>
			);
	}
}
export default Identify;