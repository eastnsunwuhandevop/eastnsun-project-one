import { Modal, Button,Input,Row, Col } from 'antd';
import React from 'react';
import Login from '../../container/login/Login';
import logo from '../../assets/img/logo.png';


class IndexLogin extends React.Component {
  state = { visible: true }
  render() {
    return (
     <Modal visible={this.state.visible}>
      <div>
       <Row type="flex" justify="center" align="middle">
      <Col>
        <img className="Login-logo" src={logo}  alt=""/>
      </Col>
    </Row>
    <hr className="Login-divider"/>
    <Row type="flex" justify="center" align="middle">
      <Col className="Login">Login</Col>
    </Row>
     <Row type="flex" justify="center" align="middle">
      <Col span={12} className="Login-email">Email</Col>
    </Row>
     <Row type="flex" justify="center" align="middle">
      <Col span={12}>
          <Input size="large" />
      </Col>
    </Row>
     <Row type="flex" justify="center" align="middle">
      <Col span={3} offset={4} className="Login-password">Password</Col>
      <Col span={9} offset={4}>
       <a className="Login-forget-password" href="">Forget password</a>
      </Col>
    </Row>
     <Row type="flex" justify="center" align="middle">
      <Col span={12}>
          <Input size="large" type="password"/>
      </Col>
    </Row>
      <Row type="flex" justify="center" align="middle">
      <Col span={12} className="Login-submit" onClick={()=>this.login()}>Submit</Col>
    </Row>
      </div>
    </Modal>  
    );
  }
}
export default IndexLogin;