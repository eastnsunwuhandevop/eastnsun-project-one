import React from "react";
import { Steps, Form, Button, Select, Input} from "antd";
import RenderForm from '../Component/form'
const FormItem = Form.Item;
const Option = Select.Option;



class Content extends React.Component {
  renderInput(e, g) {
  }

  renderForm(e){
    return (<RenderForm key={e.id} list={e} listType={this.props.listType}/>)
  }

  renderBth(e) {
    const title = e.title;
    const classname = e.class;
    const value = e.value;

    return (
      <div className="btn" key={value}>
        <Button
          className={classname}
          onClick={() => this.props.data.func({ [title]:value })}
        >
          {title}
        </Button>
      </div>
    );
  }

  renderContent(e, g) {
    switch (e.form_type) {
      case "button":
        return this.renderBth(e);
        break;
      case "input":
        return this.renderInput(e, g);
        break;
      default:
        break;
    }
  }

  renderPage(e) {
    var result = [];
    if(e.data.type === 'form'){
        result.push(this.renderForm(e))
    } else {
      e.data.form_content.map(v => {
        result.push(this.renderContent(v));
      });
    }
    return <div className={e.data.id}>{result}</div>;
  }

  render() {
    const _self = this.props;
    return <div className="main-content">{this.renderPage(_self)}</div>;
  }
}
export default Content;
