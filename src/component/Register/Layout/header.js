import React from "react";
import { Steps } from "antd";
const Step = Steps.Step;

class Header extends React.Component {
  renderProgress(e) {
    var result = [];
    for (var i = 0; i < e; i++) {
      result.push(<Step key={i}/>);
    }
    return result;
  }

  render() {
    const headerTitle = e => (
      <div className="reg-title">
        <p>{e}</p>
      </div>
    );

    const headerSubTitle = (e, p, t) => (
      <div className="reg-sub-title">
        {e ? (
          <p>
            Step {p} of {t} : {e}
          </p>
        ) : null}
      </div>
    );

    const headerProgress = (e, t) => (
      <div className="reg-progress">
        <div>
          <Steps size="small" className="reg-progress-content" current={e - 1}>
            {this.renderProgress(t)}
          </Steps>
        </div>
      </div>
    );

    const _self = this.props;

    return (
      <div>
        {headerTitle(_self.title, _self.total)}
        {headerProgress(_self.progress, _self.total)}
        {headerSubTitle(_self.subTitle, _self.progress, _self.total)}
      </div>
    );
  }
}
export default Header;
