import React from "react";
import { Steps, Form, Button, Select, Input } from "antd";
import RenderForm from "../Component/form";
const FormItem = Form.Item;
const Option = Select.Option;

class ConfirmPage extends React.Component {
    constructor(){
        super();
        this.state = {
            disabled:true
        }
    }
  renderForm(e) {
      
    return <RenderForm key={e.id} list={e} type="confirm" btn={this.renderConfirmBtn()} state={this.state.disabled} payload={e.state.payload}/>;
  }

  renderConfirmBtn = () => {
    return (<FormItem wrapperCol={{ span: 24 }}>
        <div className="controlBtn">
          <Button type="primary" htmlType="submit" className="confirmBtn">
            Confirm
          </Button>
        </div>
      </FormItem>);
  }

  renderPage(e) {
     
    var result = [];
    e.data.map(v=>{
        if (v.type === "form") {
            result.push(this.renderForm(v));
          }
    })
    

    return <div className={e.data.id}>{result}</div>;
  }

  render() {
      
    const _self = this.props;
    return <div className="main-content">{this.renderForm(_self)}</div>;
  }
}
export default ConfirmPage;
