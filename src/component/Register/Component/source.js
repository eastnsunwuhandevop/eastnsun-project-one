export const INDEX_PAGE = [
  {
    en:{
      header:[
        {
          buyer:{
            title:"Buyer",
            path:""
          },
          seller:{
            title:"Seller",
            path:""
          },
          help:{
            title:"HELP",
            path:""
          },
          signin:{
            title:"SIGN IN",
            path:""
          },
          becomeseller:{
            title:"BECOME A SELLER",
            path:""
          },
        }
      ],
      banner1:"Come here to protect your property right",
      banner2:[{
        first:{
          title:"Trust",
          subTitle:"Guranteed value and ownership",
        },
        second:{
          title:"Fair",
          subTitle:"Smart contract and consensus",
        },
        third:{
          title:"Security",
          subTitle:"Distributed ledger",
        }
      }],
      banner3:[{
        first:{
          title:"Property management"
        },
        second:{
          title:"Vehicle"
        }
      }],
      footer:[{
        signup:{
          title:"SIGN UP TO BUYER",
          path:""
        },
        signin:{
          title:"BECOME A SELLER",
          path:""
        },
        switch:{
          title:"中文",
          path:""
        },
        realestate:{
          title:"My Real Estate",
          path:""
        },
        about:{
          title:"Our story",
          path:""
        },
        vehicle:{
          title:"My Vehicle",
          path:""
        },
        careers:{
          title:"Careers",
          path:""
        },
      }],
    }
  }
]

export const HEADER = [
  {
    title: "Choose your identify"
  },
  {
    title: "Choose your profession"
  },
  {
    title: "Complete you Details",
    progress: "1",
    total: "5",
    subTitle: "Enter Your Business Information"
  },
  {
    title: "Complete you Details",
    progress: "2",
    total: "5",
    subTitle: "Enter Your Business Credit Card/PayPal"
  },
  {
    title: "Complete you Details",
    progress: "3",
    total: "5",
    subTitle: "Enter Your Address"
  },
  {
    title: "Complete you Details",
    progress: "4",
    total: "5",
    subTitle: "Enter Administrator Information"
  },
  {
    title: "Complete you Details",
    progress: "5",
    total: "5",
    subTitle: "Confirm of Details"
  }
];

export const FORM_LIST = e => [
  {
    id: "identify-profession",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Buyer",
        value: "identify_buyer",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Seller",
        value: "identify_seller",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Company",
        value: "identify_company",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Government Agencies and Industry Bodies",
        value: "identify_gov",
        form_type: "button",
        class: "identifyBtn identifyBtnSmall"
      }
    ]
  },
  {
    id: "identify-profession",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Agency",
        value: "profession_agency",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Lawyer",
        value: "profession_lawyer",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Broker",
        value: "profession_broker",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Barrister",
        value: "profession_barrister",
        form_type: "button",
        class: "identifyBtn"
      }
    ]
  },
  {
    id: "step",
    title:"Business",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Company Name",
        value: "company_name",
        form_type: "input",
        msg: "Pelease type Company Name",
        required: false,
        class: ""
      },
      {
        title: "Legal Entity No",
        value: "company_len",
        form_type: "input",
        msg: "Pelease type Legal Entity No",
        required: false,
        class: ""
      },
      {
        title: "ABN",
        value: "company_abn",
        form_type: "input",
        msg: "Pelease type ABN",
        required: false,
        class: ""
      },
      {
        title: "ACN",
        value: "company_acn",
        form_type: "input",
        msg: "Pelease type ACN",
        required: false,
        class: ""
      },
      {
        title: "Phone",
        value: "company_phone",
        form_type: "input",
        msg: "Pelease type phone number",
        required: false,
        class: ""
      },
      {
        title: "Fax",
        value: "company_fax",
        form_type: "input",
        msg: "Pelease type fax phone number",
        required: false,
        class: ""
      },
      {
        title: "Email",
        value: "company_email",
        form_type: "input",
        msg: "Pelease type email address",
        required: false,
        class: ""
      }
    ]
  },
  {
    id: "step",
    title:"Payment",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Payment Type",
        value: "payment_type",
        form_type: "selection",
        msg: "Pelease type Company Name",
        children: [
          {
            title: "Credit Card Number",
            value: "credit"
          },
          {
            title: "PayPal Account Number",
            value: "paypal"
          }
        ]
      },
      {
        title: "Credit Card Number",
        value: "payment_cardnumber",
        form_type: "input",
        msg: "Pelease type Legal Entity No",
        required: false,
        class: ""
      },
      {
        title: "Expiry Date",
        value: "payment_expiry",
        form_type: "input",
        msg: "Pelease type ABN",
        required: false,
        class: ""
      }
    ]
  },
  {
    id: "step",
    title:"Postal Address",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Business Address",
        value: "business_address",
        form_type: "input",
        msg: "Pelease type Business Address"
      },
      {
        title: "Postal Address",
        value: "postal address",
        form_type: "input",
        msg: "Pelease type Postal Address"
      },
      {
        title: "Family Address",
        value: "family Address",
        form_type: "input",
        msg: "Pelease type Family Address"
      }
    ]
  },
  {
    id: "step",
    title:"Postal Administrator",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "First Name",
        value: "firstname",
        form_type: "input",
        msg: "Pelease type First Name"
      },
      {
        title: "Last Name",
        value: "lastname",
        form_type: "input",
        msg: "Pelease type Last Name"
      },
      {
        title: "Post Code",
        value: "postcode",
        form_type: "input",
        msg: "Pelease type Post Code"
      },
      {
        title: "Phone",
        value: "phone",
        form_type: "input",
        msg: "Pelease type "
      },
      {
        title: "DOB",
        value: "dob",
        form_type: "date",
        msg: "Pelease type date of birth"
      },
      {
        title: "Email",
        value: "email",
        form_type: "input",
        msg: "Pelease type your email"
      },
      {
        title: "Licsence No",
        value: "licsenceNo",
        form_type: "input",
        msg: "Pelease type Licsence No"
      },
      {
        title: "Certificate",
        value: "certificate",
        form_type: "input",
        msg: "Pelease provide Certificate"
      }
    ]
  },
];
