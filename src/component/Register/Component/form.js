import React from "react";
import { Form, Select, Input, Button, Icon, DatePicker } from "antd";
const FormItem = Form.Item;
const Option = Select.Option;
const MonthPicker = DatePicker.MonthPicker;

class RenderForm extends React.Component {
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        const steps = this.props.list.data.title;
        var payload = [values, ...payload];
        this.props.list.data.func({ [steps]: payload });
      }
    });
  };

  handleSelectChange = value => {
    this.props.form.setFieldsValue({
      note: `Hi, ${value === "male" ? "man" : "lady"}!`
    });
  };

  renderInput = (e, g) => {
    return (
      <FormItem
        label={e.title}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        key={e.value}
      >
        {g(e.value, {
          rules: [{ required: e.required, message: e.msg }]
        })(<Input disabled={this.props.state} placeholder="adasdasd" />)}
      </FormItem>
    );
  };

  renderDatePicker = (e, g) => {
    return (
      <FormItem
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        label={e.title}
        key={e.value}
      >
        {g(e.value)(<DatePicker disabled={this.props.state} />)}
      </FormItem>
    );
  };

  renderContent(e, g) {
    // console.log('renderFormItem',e);
    switch (e.form_type) {
      case "button":
        return this.renderBth(e);
        break;
      case "input":
        return this.renderInput(e, g);
        break;
      case "date":
        return this.renderDatePicker(e, g);
        break;
      default:
        break;
    }
  }

  renderForm = (e, g) => {
    var result = [];
    const listLength = e.form_content.map.length;
    e.form_content.map((v, index) => {
      result.push(this.renderContent(v, g));
    });
    return <div className={e.id}>{result}</div>;
  };

  renderConfirmForm = (e, g) => {
    const modifyBar = e => (
      <div className="modifyBar">
        <div className="modifyBar-Content">
          <p>{e}</p>
          <i class="iconfont">&#xe671;</i>
        </div>
      </div>
    );
    var result = [];
    e.map((v, index) => {
      if (v.type === "form") {
        this.renderFindIndex(v.title);
        result.push(modifyBar(v.title));
        result.push(this.renderForm(v, g));
        result.push(<hr className="Step-divider" />);
      }
    });
    return <div className="confirm-content">{result}</div>;
  };

  renderBtn = () => {
    return (
      <FormItem wrapperCol={{ span: 24 }}>
        <div className="controlBtn">
          <Button type="dashed" id="previous" onClick={this.previous}>
            <Icon type="left" />Previous
          </Button>
          <Button type="primary" htmlType="submit">
            Next<Icon type="right" />
          </Button>
        </div>
      </FormItem>
    );
  };

  renderFindIndex(e) {
      const test = 'company_abn'
    const data = this.props.list.state.payload.find(test => {
      console.log(test)
    });
  }

  render() {
    const _self = this.props;

    const { getFieldDecorator } = _self.form;
    var _list = _self.list.data;
    return (
      <Form onSubmit={this.handleSubmit}>
        {_self.type === "confirm"
          ? this.renderConfirmForm(_list, getFieldDecorator)
          : this.renderForm(_list, getFieldDecorator)}
        {_self.type === "confirm" ? _self.btn : this.renderBtn()}
      </Form>
    );
  }
}

export default Form.create()(RenderForm);
