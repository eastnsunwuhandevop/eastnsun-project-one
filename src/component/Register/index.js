import React from "react";
import "./index.css";
import Header from "./Layout/header";
import Content from "./Layout/content";
import ConfirmPage from "./Layout/confirm"

import {HEADER, FORM_LIST} from './Component/source'

class RegIndex extends React.Component {
  constructor() {
    super();
    this.state = {
      step: 0,
      payload: []
    };
    this.handleNext = this.handleNext.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
  }

  // functions
  handleNext() {
    this.setState({
      step: this.state.step + 1
    });
  }

  handlePrev() {
    this.setState({
      step: this.state.step - 1
    });
  }

  handleSetPayload(e) {
    this.setState({
      payload: [...this.state.payload, e],
      step: this.state.step + 1
    });
  }

  // render content
  renderStepTitle(_self) {
    return HEADER.map((v, index) => {
      if (_self.step === index) {
        return (
          <Header
            key={index}
            title={v.title}
            progress={v.progress}
            total={v.total}
            subTitle={v.subTitle}
          />
        );
      }
    });
  }

  renderForms(_self) {
    var result = []
    const listLength = FORM_LIST(this).length
    if(_self.step === listLength){
        return <ConfirmPage key={_self.step} data={FORM_LIST(this)} state={_self}/>;
    }else return <Content key={_self.step} data={FORM_LIST(this)[_self.step]} />;
  }

  render() {
    const _self = this.state;
    return (
      <div>
        {this.renderStepTitle(_self)}
        {this.renderForms(_self, this.state.step)}
        <div onClick={this.handleNext}>Next</div>
        <div onClick={this.handlePrev}>Pre</div>
      </div>
    );
  }
}
export default RegIndex;
