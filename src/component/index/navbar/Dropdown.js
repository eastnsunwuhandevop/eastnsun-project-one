import React, { Component } from "react";
import { Popover } from 'antd';
import wechat from '../../../assets/img/index/wechat.jpeg';
import "./Dropdown.css";

class DropdownMenu extends Component {
  render() {
    const content = this.props.source ? this.props.source : null;
    const wechatQRCODE = <img className="img" style={{width:100}} src={wechat} alt=""/>
    return (
      <div className="dropMenuContent">
        <div className="client dropBorder">
          <div className="">
            <p>{content.buyer.title}</p>
          </div>
          <div className="">
            <p>{content.seller.title}</p>
          </div>
        </div>
        <div
          className="switch dropBorder"
          onClick={() => {
            this.props.switchLang();
          }}
        >
          <i class="iconfont">&#xe601;</i> <span>{content.switch.title}</span>
        </div>
        <div className="">
          <span style={{ marginRight: 10 }}>{content.follow.title}</span>
          <a href="https://twitter.com/EastnsunTech">
            <i className="iconfont">&#xe749;</i>
          </a>
          <a href="https://www.facebook.com/Eastnsun-166372207358750/">
            <i className="iconfont">&#xe725;</i>
          </a>
          <Popover content={wechatQRCODE}>
            <i className="iconfont">&#xe615;</i>
          </Popover>
          <a href="mailto:contact@eastnsun.com">
            <i className="iconfont">&#xe602;</i>
          </a>
        </div>
      </div>
    );
  }
}

export default DropdownMenu;
