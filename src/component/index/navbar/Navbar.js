import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import DropdownMenu from './Dropdown'
import logo from '../../../assets/img/index/logo.svg';
import './Navbar.css';

class IndexPage extends Component{
    
    constructor(props){
        super(props)
        this.state = {
            drop:false
        }
    }
    handleMenu(){
        
        this.setState({
            drop:!this.state.drop
        })

        if(!this.state.drop) {
            document.getElementById('btnBuyer').className = "hideDiv";
            document.getElementById('location').className = "hideDiv";
        } else {
            document.getElementById('btnBuyer').className = "btnBuyer columnCenter";
            document.getElementById('location').className = "location";
        }
        
        
    }

    render(){
        const content = this.props.source ? this.props.source : null
        const helpTitle = e => <div style={{display:'flex'}}>
                <div className="navTitleVisi">
                    <p>{e.help.title}</p>
                </div>
                <div className="navTitleVisi">
                    <Link to={{pathname:'/login'}}><p>{e.signin.title}</p></Link>
                </div>
            </div>   

        return <div>
            <div className="navBar">
                <div className="navBarContent">
                    <div className="navBarLeft">
                        <div className="logoImage">
                            <Link to={{pathname:'/'}}><img className="img"style={{width:160}} src={logo} alt=""/></Link>
                        </div>
                        
                            <div className="navTitle">
                                <p>{content.buyer.title}</p>
                            </div>
                            <div className="navTitle">
                                <p>{content.seller.title}</p>
                            </div>
                        
                    </div>

                    <div className="navBarRight">
                        {/* {
                            this.state.drop?
                            helpTitle(content)
                            :
                            null
                        }
                        <div className="navTitle">
                            <p>{content.help.title}</p>
                        </div> */}
                        <div className="navTitle">
                            <Link to={{pathname:'/login'}}><p>{content.signin.title}</p></Link>
                        </div>
                        <div className="location" id="location">
                            <i className="iconfont">&#xe620;</i>
                        </div>
                        <Link to={{pathname:'/identify'}}>
                            <div className="btnBuyer columnCenter" id="btnBuyer">
                                <p className="title">{content.becomeseller.title}</p>
                            </div>
                        </Link>
                        <div className="menuIcon" style={{display:"none"}}  onClick={()=>this.handleMenu()}>
                            <i className="iconfont">&#xe7c5;</i>
                        </div>
                    </div>
                </div>
                
            </div>
            {
                this.state.drop?
                <div className="dropMenu">
                    <DropdownMenu source={this.props.source} switchLang={this.props.switchLang.bind(this)}/>
                </div>:
                null
            }
            
        </div>
        
    }
}

export default IndexPage