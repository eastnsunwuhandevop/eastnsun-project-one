import React, {Component} from 'react'
import './Bannersec.css';
import bannerimage_one from '../../../assets/img/index/home-pag-4_15.png';
import bannerimage_two from '../../../assets/img/index/home-pag-4_17.png';
import bannerimage_three from '../../../assets/img/index/home-pag-4_19.png';

class Bannersec extends Component{
    render(){
        const content = this.props.source
        return <div className="bannersec">
            <div className="bannersecContent">
                <div><img className="img" src={bannerimage_one} alt=""/></div>
                <div className="title-content">
                    <div><p className="title">{content.first.title}</p></div>
                    <div><p className="subTitle">{content.first.subTitle}</p></div>
                </div>
            </div>
            <div className="bannersecContent">
                <div><img className="img" src={bannerimage_two} alt=""/></div>
                <div className="title-content">
                    <div><p className="title">{content.second.title}</p></div>
                    <div><p className="subTitle">{content.second.subTitle}</p></div>
                </div>
            </div>
            <div className="bannersecContent">
                <img className="img" src={bannerimage_three} alt=""/>
                <div className="title-content">
                    <div><p className="title">{content.third.title}</p></div>
                    <div><p className="subTitle">{content.third.subTitle}</p></div>
                </div>
            </div>
        </div>
    }
}

export default Bannersec