import React, {Component} from 'react'
import './Bannerthird.css';
import property from '../../../assets/img/index/home-pag-4_24.png';
import car from '../../../assets/img/index/home-pag-4_26.png';
import Food from '../../../assets/img/index/WechatIMG31.jpeg';

class Bannerthird extends Component{
    render(){
        const content = this.props.source ? this.props.source : null
        return <div className="bannerthird">
            <div className="content">
                <div><img className="img" src={property} alt=""/></div>
                <div className="title-content">
                    <div><p className="title">{content.first.title}</p></div>
                </div>
            </div>
            <div className="content">
                <div><img className="img" src={car} alt=""/></div>
                <div className="title-content">
                    <div><p className="title">{content.second.title}</p></div>
                </div>
            </div>
            <div className="content">
                <div><img className="img" src={Food} alt=""/></div>
                <div className="title-content">
                    <div><p className="title">{content.third.title}</p></div>
                </div>
            </div>
        </div>
    }
}

export default Bannerthird