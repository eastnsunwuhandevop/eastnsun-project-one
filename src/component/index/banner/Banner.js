import React, {Component} from 'react'
import './Banner.css';
import bannerimage from '../../../assets/img/index/home-pag-4_11.png';

class Banner extends Component{
    render(){
        const content = this.props.source
        
        return <div className="banner">
            <div className="bannerImage">
                <img className="bannerImg" src={bannerimage} alt=""/>
            </div>
            <div className="bannerTitle">
                <p>{content}</p>
            </div>
        </div>
    }
}

export default Banner