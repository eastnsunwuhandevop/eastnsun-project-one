import React, {Component} from 'react'
import { Link } from 'react-router-dom'
import { Popover } from 'antd';
import './Footer.css';
import logo from '../../../assets/img/index/logo.svg';
import wechat from '../../../assets/img/index/wechat.jpeg';



class Footer extends Component{
    render(){
        const content = this.props.source ? this.props.source : null

        const wechatQRCODE = <img className="img" style={{width:100}} src={wechat} alt=""/>
        return <div className="footer">
            <div className="footerContent">
                <div className="subBanner clearBorder logoBtn">
                    <div className="width3 columnStart">
                        <Link to={{pathname:'/'}}><img className="img" src={logo} alt=""/></Link>
                    </div>
                    <div className="width3 columnCenter">
                        {/* <div className="btnBuyer titleCenter">
                        <Link to={{pathname:'/identify'}}><p className="title">{content.signup.title}</p></Link>
                        </div> */}
                    </div>
                    <div className="width3 columnEnd">
                        {/* <div className="btnSeller titleCenter">
                        <Link to={{pathname:'/identify'}}><p className="title">{content.signin.title}</p></Link>
                        </div> */}
                    </div>
                </div>

                <div className="subBanner white mobileBanner clearBorder">
                    <div className="width3 columnStart">
                        <div className="mobileBorder" onClick={()=>{this.props.switchLang()}}><i className="iconfont">&#xe601;</i>{content.switch.title}</div>
                        {/* <div className="help mobileBorder" style={{display:'none'}}><i className="iconfont">&#xe72c;</i>{content.help.title}</div> */}
                        <div className="iconlist mobileBorder">
                            <a href="https://twitter.com/EastnsunTech"><i className="iconfont">&#xe749;</i></a>
                            <a href="https://www.facebook.com/Eastnsun-166372207358750/"><i className="iconfont">&#xe725;</i></a>
                            <Popover content={wechatQRCODE}>
                                <i className="iconfont">&#xe615;</i>
                            </Popover>
                            <a href="mailto:contact@eastnsun.com"><i className="iconfont">&#xe602;</i></a>
                        </div>
                    </div>
                    <div  className="width3 columnCenter">
                        <div className="product">
                            <p className="mobileBorder">{content.realestate.title}</p>
                            <p  className="mobileBorder">{content.vehicle.title}</p>
                            <p  className="mobileBorder">{content.food.title}</p>
                        </div>
                        
                    </div>
                    <div className="width3 columnEnd">
                        <div className="story">
                        <Link to={{pathname:'/ourstory'}}>
                            <p  className="mobileBorder">{content.about.title}</p>
                        </Link>
                        <Link to={{pathname:'/career'}}>
                            <p className="mobileBorder">{content.careers.title}</p>
                        </Link>
                        </div>
                    </div>
                </div>

                <div className="hideBorder white subBanner mobileBanner term">
                    <div className="width3 flexStart">
                        <p>©2018 Eastnsun,All Rights Reserved.</p>
                    </div>
                    <div className="width3 columnCenter">
                        <p className="product">Privacy Terms</p>
                    </div>
                    <div className="width3 columnEnd">
                    </div>
                    
                    
                    <p></p>
                </div>
            </div>
        </div>
    }
}

export default Footer