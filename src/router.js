import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route } from 'react-router-dom';
import Login from './container/login/Login';
import Identify from './container/identify/Identify';
import Profession from './container/profession/Profession';
import Detail from './container/detail/Detail';

import RegIndex from './container/register/index'

const supportsHistory = 'pushState' in window.history;
ReactDOM.render(
    <BrowserRouter>
        <div>
            
            <Route exact path='/' component={Identify}></Route>
              <Route exact path='/Profession' component={Profession}></Route>
              <Route exact path='/Detail' component={Detail}></Route>
              <Route exact path='/register' component={RegIndex}></Route>
           
        </div>
    </BrowserRouter>
, document.getElementById('root'));