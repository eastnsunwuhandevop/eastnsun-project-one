import React from 'react';
import { Input } from 'antd';
import './Sio.css';
const Search = Input.Search;
class Searchs extends React.Component {
      render(){
          return(
            <div className="Sio-search-container">
              <Search className="Sio-search" placeholder="input search text" enterButton="Search" />
            <div className="Sio-text">Can't find?</div>
        </div>
          );
      }
}
export default Searchs;