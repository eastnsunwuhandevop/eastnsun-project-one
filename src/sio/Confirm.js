import React from 'react';
import {Input,Icon} from 'antd';
class Confirm extends React.Component {
    render(){
        return(
        <div className="Sio-info2-container">    
            <div className="Sio-title-container">
                <div className="Sio-modify">Property Infomation</div>
                <div><Icon type="form" style={{ fontSize: 20, color: '#08c' }}/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Land   Volume</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Folio</div>
                <div className=".Sio-input"><Input/></div>
            </div>
            <div className="Sio-content-container">
                <div className="Sio-lable">Lot</div>
                <div className=".Sio-input"><Input/></div>
            </div>
        </div>    
        );
    }
}
export default Confirm;