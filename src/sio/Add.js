import React from 'react';
import {Input,Select,Button,Icon} from 'antd';
import Confirm from './Confirm';
const Option = Select.Option;
const Search = Input.Search;
class Add extends React.Component {
     render(){
         return(
            <div className="Sio-container">
            <div className="Sio-title-container">
                <div className="Sio-confirm">Confirm Property Details below</div>
                <div className="Sio-update">Update Address</div>
            </div>    
            <div className="Sio-info-container">
                <div className="Sio-input-container">
                  <div className="Sio-input-left-container">
                    <div className="Sio-lable">Unit</div>
                    <Input className="Sio-input"/>
                   </div>  
                </div>
                <div className="Sio-input-container">
                <div className="Sio-input-left-container">  
                   <div className="Sio-lable">Street Name</div>
                     <Input className="Sio-input"/>
                   </div>
                </div>   
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-left-container">
                     <div className="Step-lable">Street Number</div>
                    <Input className="Sio-input"/>
                </div>  
                <div className="Sio-input-left-container">
                    <div className="Sio-lable">Suburb</div>
                    <Search
                        onSearch={value => console.log(value)}
                        style={{ width: 120 }}
                        />
                </div>
            </div>
            <div className="Sio-info-container">
                <div className="Sio-input-left-container">
                     <div className="Step-lable">Postcode</div>
                    <Input className="Sio-input"/>
                </div>  
                <div className="Sio-input-left-container">
                    <div className="Sio-lable">State</div>
                    <Select
                        showSearch
                        style={{ width: 120 }}
                        optionFilterProp="children"
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                        <Option value="jack">VIC</Option>
                        <Option value="lucy">NSW</Option>
                        <Option value="tom">TAS</Option>
                        <Option value="tom">NT</Option>
                        <Option value="tom">WA</Option>
                        <Option value="tom">QLD</Option>
                        <Option value="tom">SA</Option>
                    </Select>
                </div>
            </div>
            <Button className="Sio-add" type="primary"size="large">ADD</Button>
            <Confirm />
         </div>   
         );
     }
}
export default Add;