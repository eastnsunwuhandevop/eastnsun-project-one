export const INDEX_PAGE = {
  en: {
    header: {
      buyer: {
        title: "",
        path: ""
      },
      seller: {
        title: "",
        path: ""
      },
      help: {
        title: "HELP",
        path: ""
      },
      signin: {
        title: "SIGN IN",
        path: ""
      },
      becomeseller: {
        title: "BECOME A MEMBER",
        path: ""
      },
      follow: {
        title: "Follow Us",
        path: ""
      },
      switch: {
        title: "中文",
        path: ""
      }
    },
    banner1: "Come here to protect your property right",
    banner2: {
      first: {
        title: "Trust",
        subTitle: "Guranteed value and ownership"
      },
      second: {
        title: "Fair",
        subTitle: "Smart contract and consensus"
      },
      third: {
        title: "Security",
        subTitle: "Distributed ledger"
      }
    },
    banner3: {
      first: {
        title: "Property management"
      },
      second: {
        title: "Vehicle"
      },
      third: {
        title: "Food"
      }
    },
    footer: {
      signup: {
        title: "SIGN UP TO BUYER",
        path: ""
      },
      signin: {
        title: "BECOME A SELLER",
        path: ""
      },
      switch: {
        title: "中文",
        path: ""
      },
      realestate: {
        title: "My Real Estate",
        path: ""
      },
      about: {
        title: "Our story",
        path: ""
      },
      vehicle: {
        title: "My Vehicle",
        path: ""
      },
      careers: {
        title: "Careers",
        path: ""
      },
      help: {
        title: "HELP",
        path: ""
      },
      food: {
        title: "My Food",
        path: ""
      }
    }
  },
  cn: {
    header: {
      buyer: {
        title: "",
        path: ""
      },
      seller: {
        title: "",
        path: ""
      },
      help: {
        title: "帮助",
        path: ""
      },
      signin: {
        title: "登陆",
        path: ""
      },
      becomeseller: {
        title: "成为会员",
        path: ""
      },
      follow: {
        title: "关注我们",
        path: ""
      },
      switch: {
        title: "ENGLISH",
        path: ""
      }
    },
    banner1: "您可以在这里保护您的财产权",
    banner2: {
      first: {
        title: "信任",
        subTitle: "保证您的价值和所有权"
      },
      second: {
        title: "公平",
        subTitle: "智能合约和共识"
      },
      third: {
        title: "安全",
        subTitle: "分布式账本"
      }
    },
    banner3: {
      first: {
        title: "物产管理"
      },
      second: {
        title: "汽车"
      },
      third: {
        title: "食品"
      }
    },
    footer: {
      signup: {
        title: "登记为买家",
        path: ""
      },
      signin: {
        title: "成为卖家",
        path: ""
      },
      switch: {
        title: "ENGLISH",
        path: ""
      },
      realestate: {
        title: "房产",
        path: ""
      },
      about: {
        title: "我们的故事",
        path: ""
      },
      vehicle: {
        title: "汽车",
        path: ""
      },
      careers: {
        title: "职业规划",
        path: ""
      },
      help: {
        title: "帮助",
        path: ""
      },
      food: {
        title: "食品",
        path: ""
      }
    }
  }
};

export const HEADER = [
  {
    title: "Choose your identify"
  },
  {
    title: "Choose your profession"
  },
  {
    title: "Complete you Details",
    progress: "1",
    total: "5",
    subTitle: "Enter Your Business Information"
  },
  {
    title: "Complete you Details",
    progress: "2",
    total: "5",
    subTitle: "Enter Your Business Credit Card/PayPal"
  },
  {
    title: "Complete you Details",
    progress: "3",
    total: "5",
    subTitle: "Enter Your Address"
  },
  {
    title: "Complete you Details",
    progress: "4",
    total: "5",
    subTitle: "Enter Administrator Information"
  },
  {
    title: "Complete you Details",
    progress: "5",
    total: "5",
    subTitle: "Confirm of Details"
  }
];

export const FORM_LIST = e => [
  {
    id: "identify-profession",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Buyer",
        value: "identify_buyer",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Seller",
        value: "identify_seller",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Company",
        value: "identify_company",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Government Agencies and Industry Bodies",
        value: "identify_gov",
        form_type: "button",
        class: "identifyBtn identifyBtnSmall"
      }
    ]
  },
  {
    id: "identify-profession",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Agency",
        value: "profession_agency",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Lawyer",
        value: "profession_lawyer",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Broker",
        value: "profession_broker",
        form_type: "button",
        class: "identifyBtn"
      },
      {
        title: "Barrister",
        value: "profession_barrister",
        form_type: "button",
        class: "identifyBtn"
      }
    ]
  },
  {
    id: "step",
    title: "Business",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Company Name",
        value: "company_name",
        form_type: "input",
        msg: "Pelease type Company Name",
        required: false,
        class: ""
      },
      {
        title: "Legal Entity No",
        value: "company_len",
        form_type: "input",
        msg: "Pelease type Legal Entity No",
        required: false,
        class: ""
      },
      {
        title: "ABN",
        value: "company_abn",
        form_type: "input",
        msg: "Pelease type ABN",
        required: false,
        class: ""
      },
      {
        title: "ACN",
        value: "company_acn",
        form_type: "input",
        msg: "Pelease type ACN",
        required: false,
        class: ""
      },
      {
        title: "Phone",
        value: "company_phone",
        form_type: "input",
        msg: "Pelease type phone number",
        required: false,
        class: ""
      },
      {
        title: "Fax",
        value: "company_fax",
        form_type: "input",
        msg: "Pelease type fax phone number",
        required: false,
        class: ""
      },
      {
        title: "Email",
        value: "company_email",
        form_type: "input",
        msg: "Pelease type email address",
        required: false,
        class: ""
      }
    ]
  },
  {
    id: "step",
    title: "Payment",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Payment Type",
        value: "payment_type",
        form_type: "selection",
        msg: "Pelease type Company Name",
        children: [
          {
            title: "Credit Card Number",
            value: "credit"
          },
          {
            title: "PayPal Account Number",
            value: "paypal"
          }
        ]
      },
      {
        title: "Credit Card Number",
        value: "payment_cardnumber",
        form_type: "input",
        msg: "Pelease type Legal Entity No",
        required: false,
        class: ""
      },
      {
        title: "Expiry Date",
        value: "payment_expiry",
        form_type: "input",
        msg: "Pelease type ABN",
        required: false,
        class: ""
      }
    ]
  },
  {
    id: "step",
    title: "Postal Address",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "Business Address",
        value: "business_address",
        form_type: "input",
        msg: "Pelease type Business Address"
      },
      {
        title: "Postal Address",
        value: "postal address",
        form_type: "input",
        msg: "Pelease type Postal Address"
      },
      {
        title: "Family Address",
        value: "family Address",
        form_type: "input",
        msg: "Pelease type Family Address"
      }
    ]
  },
  {
    id: "step",
    title: "Postal Administrator",
    type: "form",
    func: e.handleSetPayload.bind(e),
    form_content: [
      {
        title: "First Name",
        value: "firstname",
        form_type: "input",
        msg: "Pelease type First Name"
      },
      {
        title: "Last Name",
        value: "lastname",
        form_type: "input",
        msg: "Pelease type Last Name"
      },
      {
        title: "Post Code",
        value: "postcode",
        form_type: "input",
        msg: "Pelease type Post Code"
      },
      {
        title: "Phone",
        value: "phone",
        form_type: "input",
        msg: "Pelease type "
      },
      {
        title: "DOB",
        value: "dob",
        form_type: "date",
        msg: "Pelease type date of birth"
      },
      {
        title: "Email",
        value: "email",
        form_type: "input",
        msg: "Pelease type your email"
      },
      {
        title: "Licsence No",
        value: "licsenceNo",
        form_type: "input",
        msg: "Pelease type Licsence No"
      },
      {
        title: "Certificate",
        value: "certificate",
        form_type: "input",
        msg: "Pelease provide Certificate"
      }
    ]
  }
];

export const OUR = {
  head:{
    en:{
      title:"We focus on Regtech",
      subTitle:"Not only fintech"
    },
    cn:{
      title:"我们专注于监管科技",
      subTitle:"不仅是金融科技"
    }
  },
  en: {
    title: "OUR TEAM",
    subTitle: "The ones who run",
    list: [
      {
        name: "ZHI LI",
        title: "CEO & Founder",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_07.png"
      },
      {
        name: "ROBORT MAGISTRELLI",
        title: "CTO & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_09.png"
      },
      {
        name: "NINGHAI WANG",
        title: "CIO & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/wang-ninghai_11.png"
      },
      {
        name: "QING MU",
        title: "CFO & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_13.png"
      },
      {
        name: "PING TAN",
        title: "CMO-CANADA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_19.png"
      },
      {
        name: "DE DAI",
        title: "CMO-CANADA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_21.png"
      },
      {
        name: "TIANQI ZHAO",
        title: "CMO-AUSTRALIA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_23.png"
      },
      {
        name: "WEN YANG",
        title: "CMO-AUSTRALIA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_24.png"
      },
      {
        name: "AHSAN KHAN",
        title: "CXO & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_11.png"
      },
      {
        name: "RUGUO ZHOU",
        title: "CXO-CHINA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_30.png"
      },
      {
        name: "DING LI",
        title: "COO-CHINA & Partner",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_29.png"
      }
    ]
  },
  cn: {
    title: "我们的团队",
    subTitle: "",
    list: [
      {
        name: "李治",
        title: "首席执行官 & 创始人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_07.png"
      },
      {
        name: "ROBORT MAGISTRELLI",
        title: "首席技术官 & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_09.png"
      },
      {
        name: "王宁海",
        title: "首席信息官 & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/wang-ninghai_11.png"
      },
      {
        name: "穆青",
        title: "首席财务官 & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_13.png"
      },
      {
        name: "谭平",
        title: "首席市场官(加) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_19.png"
      },
      {
        name: "戴德",
        title: "首席市场官(加) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_21.png"
      },
      {
        name: "赵天祺",
        title: "首席市场官(澳) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_23.png"
      },
      {
        name: "杨文",
        title: "首席市场官(澳) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_24.png"
      },
      {
        name: "AHSAN KHAN",
        title: "首席体验官 & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_11.png"
      },
      {
        name: "周汝国",
        title: "首席体验官(中) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_30.png"
      },
      {
        name: "李丁",
        title: "首席运营官(中) & 合伙人",
        imagePath:
          "http://cdn.eastnsun.com.s3-website-ap-southeast-2.amazonaws.com/resources/images/company-1920_29.png"
      }
    ]
  }
};

export const ABOUT = {
  en: {
    title: "OUR COMPANY",
    storyText: {
      title: "Our story",
      text:
        "Eastnsun is an Australian multinational technology company based in Melbourne, Australia that was founded by Zhi Li, a former Accenture enterprise architect, in March 2011 in Canada. Eastnsun has grown from a small IT consulting firm, founded in Wuhan City of China in 2005, into a global technology company delicated to helping organisations protect and enhance enterprise value in an increasingly dynamic, regulatory environment. Eastnsun is specialising in Big Data, Blockchain, Internet of Thing, AI."
    },
    visionText: {
      title: "Vision",
      text:
        "Manage My Whole Life Records for all the people in a secure environment."
    },
    missionText: {
      title: "Mission",
      text:
        "Re-establish the trust through leading blockchain based risk and compliance management platform."
    }
  },
  cn: {
    title: "关于我们",
    storyText: {
      title: "我们的故事",
      text:
        "以太势是一家位于澳大利亚墨尔本的跨国科技公司，公司创始人李治先生是前埃森哲企业架构师。他于2011年3月在加拿大创立了以太势科技公司。以太势最初于2005年成立于中国湖北省武汉市，经过十多年的发展，以太势从一家小型的IT咨询公司成长为一家致力于帮助企业在多变的监管环境中保护和加强企业价值的跨国IT公司。以太势公司专门服务于大数据、区块链、物联网和人工智能领域。"
    },
    visionText: {
      title: "我们的愿景",
      text: "在安全的环境中为所有人管理“我的一生“信息系统。"
    },
    missionText: {
      title: "我们的使命",
      text: "通过领先的以区块链为主体的风险及合规管理平台去重建信任"
    }
  }
};


